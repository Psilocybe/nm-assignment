﻿///
/// \file ExplicitUpwind.cpp
///
/// \author Bartłomiej Szostek
/// \date October 2016
///
/// \brief Implementation of explicit upwind solver.
//////////////////////////////////////////////////////////////////////////

#include "ExplicitUpwind.h"

namespace advection
{
	namespace scheme
	{
		bool ExplicitUpwind::isStable(double deltaTime)
		{
			return calculateCFL(deltaTime) <= 1.0;
		}

		std::vector<double> ExplicitUpwind::calculateStep(std::vector<double> previousStep)
		{
			std::vector<double> currentStep(gridSize);
			currentStep[0] = lowerBoundaryValue;
			currentStep[gridSize - 1] = upperBoundaryValue;
			for (auto g = 1; g < gridSize - 1; g++)
			{
				auto f_i = previousStep[g];
				auto f_i_minus_1 = previousStep[g - 1];
				currentStep[g] = f_i + cfl * (f_i_minus_1 - f_i);
			}
			return currentStep;
		}

		ExplicitUpwind::ExplicitUpwind(IO::AdvectionConfiguration& configuration)
			: IScheme(configuration)
		{
		}

		double ExplicitUpwind::getMaxDeltaSpaceForStableScheme() const
		{
			auto maxDeltaTime = deltaSpace / acceleration;
			return maxDeltaTime;
		}
	}
}
