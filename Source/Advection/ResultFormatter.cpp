﻿///
/// \file ResultFormatter.cpp
///
/// \author Bartłomiej Szostek
/// \date November 2016
///
/// \brief Methods implementation of result formatter.
//////////////////////////////////////////////////////////////////////////

#include "ResultFormatter.h"

#include <string>
#include <iomanip>

namespace advection
{
	namespace IO
	{
		void ResultFormatter::saveValues(int position)
		{
			outputFile << std::scientific << std::setprecision(resultPrecision);
			for (size_t i = 0; i < analyticalValues.size(); ++i)
			{
				outputFile << analyticalValues[i][position] << delimiter;
				outputFile << numericalValues[i][position] << delimiter;
			}
		}

		ResultFormatter::ResultFormatter(
			std::vector<double> grid,
			std::vector<std::vector<double>> analyticalValues,
			std::vector<std::vector<double>> numericalValues,
			std::vector<double> timeLevels,
			const std::string& fileName)
			: IFormatter(fileName), grid(grid), timeLevels(timeLevels),
			numericalValues(numericalValues), analyticalValues(analyticalValues)
		{
			if (timeLevels.size() != numericalValues.size() || 
				timeLevels.size() != analyticalValues.size() ||
				numericalValues.size() != analyticalValues.size())
				throw std::invalid_argument("Vector sizes should match.");
		}

		void ResultFormatter::save()
		{
			for (size_t i = 0; i < grid.size(); i++)
			{
				outputFile << std::fixed;
				outputFile << std::setprecision(1) << grid.at(i) << delimiter;
				saveValues(i);
				outputFile << std::endl;
			}
		}
	}
}
