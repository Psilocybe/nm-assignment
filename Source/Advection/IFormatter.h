﻿///
/// \file IFormatter.h
///
/// \author Bartłomiej Szostek
/// \date November 2016
///
/// \brief Abstract output formatter declaration.
//////////////////////////////////////////////////////////////////////////

#ifndef _I_FORMATTER_H
#define _I_FORMATTER_H

#include <fstream>

namespace advection
{
	namespace IO
	{
		/// Interface for dealing with formatting and writing to output files.
		class IFormatter
		{
		protected:
			/// Output file stream for results.
			std::fstream outputFile;

		public:
			/// Initializes formatter to write to specified file.
			///
			/// @param fileName Output result file name.
			explicit IFormatter(std::string fileName);

			/// Closes file stream.
			virtual ~IFormatter();

			/// Pure virtual method invoked for writing to output file.
			virtual void save() = 0;
		};
	}
}

#endif
