﻿///
/// \file AdvectionConfiguration.cpp
///
/// \author Bartłomiej Szostek
/// \date November 2016
///
/// \brief Implementation of configuration parser.
///
/// Implementations of functions that are parsing configuration files
/// provided by user. Each parameter is read from config reader and 
/// assigned to proper AdvectionConfiguration fields.
//////////////////////////////////////////////////////////////////////////

#include "AdvectionConfiguration.h"
#include "ConfigurationReader.h"
#include "Functions.h"

namespace advection
{
	namespace IO
	{
		AdvectionConfiguration readConfig(ConfigurationReader& reader)
		{
			AdvectionConfiguration config;
			config.gridSize = reader.getParameter("gridSize");
			config.lowerBoundary = reader.getParameter("lowerBoundary");
			config.upperBoundary = reader.getParameter("upperBoundary");
			config.lowerBoundaryValue = reader.getParameter("lowerBoundaryValue");
			config.upperBoundaryValue = reader.getParameter("upperBoundaryValue");
			config.acceleration = reader.getParameter("acceleration");
			std::string initFunction = reader.getParameter("initFunction");
			if (initFunction == "signum")
				config.initFunction = &analytical::signumFunction;
			else if (initFunction == "exponential")
				config.initFunction = &analytical::exponentialFunction;
			else
				throw std::invalid_argument("Unknown initialization function name.");
			std::vector<double> timeLevels = reader.getParameter("timeLevels");
			config.timeLevels = timeLevels;
			return config;
		}
	}
}
