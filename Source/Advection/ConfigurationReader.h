﻿///
/// \file ConfigurationReader.h
///
/// \author Bartłomiej Szostek
/// \date November 2016
///
/// \brief Configuration reader mechanism declaration.
//////////////////////////////////////////////////////////////////////////

#ifndef _CONFIGURATION_READER_H
#define _CONFIGURATION_READER_H

#include "Parameter.h"

#include <map>
#include <stdexcept>

namespace advection
{
	namespace IO
	{
		/// Reads key-value pair parameters from specified configuration.
		class ConfigurationReader
		{
			/// Reader skips lines which begins with this character.
			const char commentCharacter = '#';

			/// Separates name of parameter (key) from its value.
			const char keyValueDelimiter = '=';

			/// Unique options read from configuration.
			std::map<std::string, std::string> options;

			/// Reads parameters from given stream.
			/// Key-value pair parameters are read line by line and stored inside private map.
			///
			/// @param stream Input stream from which reader will fetch parameters.
			/// @throws logic_error When parameters in configuration are duplicated.
			void readConfiguration(std::istream &stream);

			/// Transforms key to internal form.
			///
			/// @param key Original key.
			/// @return Transformed key.
			std::string makeKey(std::string key) const;

		public:
			/// Initializes reader with input stream.
			///
			/// @param stream Input stream from which configuration will be fetched.
			explicit ConfigurationReader(std::istream &stream);

			/// Initializes reader with input file.
			/// Attempts to open configuration file and parse its content.
			///
			/// @param configFileName Name of file with configuration.
			/// @throws runtime_error When failed to open configuration file.
			explicit ConfigurationReader(const std::string& configFileName);

			/// Checks if specific key is present among configuration.
			///
			/// @param key Name of parameter.
			/// @return True if key was found, false otherwise.
			bool parameterExists(const std::string& key) const;

			/// Retrieves value of given key.
			/// Tries to lookup value by given parameter name.
			///
			/// @param key Name of parameter.
			/// @return Parameter String value of parameter casted to specific type.
			/// @throws out_of_range When key is not found inside configuration.
			Parameter getParameter(const std::string key) const;
		};
	}
}

#endif
