﻿///
/// \file Functions.cpp
///
/// \author Bartłomiej Szostek
/// \date November 2016
///
/// \brief Implementation of analytical solutions for advection equation.
///
/// Implementations of functions that are solving analytically advection
/// equation and internally used functions.
//////////////////////////////////////////////////////////////////////////

#include "functions.h"

#include <cmath>

namespace advection
{
	namespace analytical
	{
		double signumFunction(const double x, const double time, const double acceleration)
		{
			auto abscissa = x - acceleration * time;
			return 0.5 * (signum(abscissa) + 1.0);
		}

		double exponentialFunction(const double x, const double time, const double acceleration)
		{
			auto abscissa = x - acceleration * time;
			return 0.5 * exp(-1.0 * abscissa * abscissa);
		}
	}
}
