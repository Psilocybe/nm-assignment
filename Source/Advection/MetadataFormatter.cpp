﻿///
/// \file MetadataFormatter.cpp
///
/// \author Bartłomiej Szostek
/// \date November 2016
///
/// \brief Methods implementation of metadata formatter.
//////////////////////////////////////////////////////////////////////////

#include "MetadataFormatter.h"

namespace advection
{
	namespace IO
	{
		MetadataFormatter::MetadataFormatter(
			size_t gridSize,
			double deltaTime,
			double deltaSpace,
			std::vector<tool::Norms> norms,
			std::string scheme,
			std::string& fileName)
			: IFormatter(fileName),
			scheme(scheme), gridSize(gridSize), deltaTime(deltaTime), deltaSpace(deltaSpace), norms(norms)
		{
		}

		void MetadataFormatter::save()
		{
			outputFile << "Scheme= " << scheme << std::endl; 
			outputFile << "Delta(t)= " << deltaTime << std::endl;
			outputFile << "Delta(x)= " << deltaSpace << std::endl;
			outputFile << "Grid size= " << gridSize << std::endl;
			auto sumOne = 0.0, sumTwo = 0.0, sumUniform = 0.0;
			for (size_t i = 0; i < norms.size(); ++i)
			{
				outputFile << "(T=" << i << ") Norm 1= " << norms.at(i).one << std::endl;
				outputFile << "(T=" << i << ") Norm 2= " << norms.at(i).two << std::endl;
				outputFile << "(T=" << i << ") Uniform norm= " << norms.at(i).uniform << std::endl;
				sumOne += norms.at(i).one; sumTwo += norms.at(i).two; sumUniform += norms.at(i).uniform;
			}
			outputFile << "Average norm 1= " << sumOne / norms.size() << std::endl;
			outputFile << "Average norm 2= " << sumTwo / norms.size() << std::endl;
			outputFile << "Average uniform norm= " << sumUniform /norms.size()<< std::endl;
		}
	}
}
