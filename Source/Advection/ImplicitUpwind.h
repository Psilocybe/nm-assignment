///
/// \file ImplicitUpwind.h
///
/// \author Bartłomiej Szostek
/// \date October 2016
///
/// \brief Declaration of advection solver based on implicit upwind scheme.
//////////////////////////////////////////////////////////////////////////

#ifndef _IMPLICIT_UPWIND_H
#define _IMPLICIT_UPWIND_H

#include "IScheme.h"

namespace advection
{
	namespace scheme
	{
		/// @brief Advection equation solver implementing implicit upwind scheme.
		///
		/// This solver forward-time backward-space scheme which is actually 
		/// 2-diagonal matrix, which can be solved using forward substitution.
		class ImplicitUpwind :
			public IScheme
		{
			/// Solves PDE for positive CFL numbers.
			///
			/// @param currentStep Vector for currently calculated grid points.
			/// @param previousStep Vector of values calculated in previous time step.
			void forward(std::vector<double>& currentStep, const std::vector<double>& previousStep) const;

			/// Solves PDE for negative CFL numbers.
			///
			/// @param currentStep Vector for currently calculated grid points.
			/// @param previousStep Vector of values calculated in previous time step.
			void backward(std::vector<double>& currentStep, const std::vector<double>& previousStep) const;

			/// Performs results correction (shifting) for negative CFL number.
			void afterSolving() override;

		protected:
			/// Checks if scheme is stable for given step in time and configuration.
			///
			/// @param deltaTime Step in time.
			/// @return True for stable scheme, false otherwise.
			bool isStable(double deltaTime) override;

			/// Calculates values for next time step.
			///
			/// @param previousStep Vector of values from previous step.
			/// @return Calculated vector.
			std::vector<double> calculateStep(std::vector<double> previousStep) override;

		public:
			/// Initializes scheme by configuration parameters.
			///
			/// @param configuration Configuration parameters for simulation.
			explicit ImplicitUpwind(IO::AdvectionConfiguration& configuration);
			
			/// Get maximum value of step in space for which scheme is stable.
			///
			/// @return Maximum step in space for implicit upwind.
			double getMaxDeltaSpaceForStableScheme() const override;
		};
	}
}

#endif
