﻿///
/// \file DeltaTimeTuner.cpp
///
/// \author Bartłomiej Szostek
/// \date October 2016
///
/// \brief Implementation of delta time tuner.
//////////////////////////////////////////////////////////////////////////

#include "DeltaTimeTuner.h"

#include <iostream>

namespace advection
{
	namespace tool
	{
		DeltaTimeTuner::DeltaTimeTuner(scheme::IScheme* scheme, int iterations)
			: scheme(scheme), iterations(iterations),
			lowestTwoNormAvg(std::numeric_limits<double>::infinity()), bestDeltaTime(0.0)
		{
		}

		double DeltaTimeTuner::getAverageTwoNorm(std::vector<Norms> errorNorms) const
		{
			auto sum = std::accumulate(errorNorms.begin(), errorNorms.end(), 0.0,
				[](double sum, const tool::Norms& norm) { return sum + norm.two; });
			return sum / errorNorms.size();
		}

		void DeltaTimeTuner::tune()
		{
			auto maxDeltaTime = scheme->getMaxDeltaSpaceForStableScheme();
			auto deltaTimeStep = maxDeltaTime / iterations;
			try
			{
				for (auto deltaTime = deltaTimeStep; deltaTime <= maxDeltaTime; deltaTime += deltaTimeStep)
				{
					scheme->solve(deltaTime);
					auto average = getAverageTwoNorm(scheme->getNorms());
					if (average < lowestTwoNormAvg)
					{
						bestDeltaTime = deltaTime;
						bestNorms = scheme->getNorms();
						lowestTwoNormAvg = average;
						analyticalValues = scheme->getAnalyticalValues();
						numericalValues = scheme->getNumericalValues();
					}
				}
			}
			catch (std::out_of_range& err)
			{
				std::cerr << "Breaking tuning operation." << std::endl;
				std::cerr << "Logic error: " << typeid(err).name() << std::endl;
				std::cerr << err.what() << std::endl;
			}
			std::cout << "Best deltaTime= " << bestDeltaTime << std::endl;
			std::cout << "Smallest error err= " << lowestTwoNormAvg << std::endl;
		}

		double DeltaTimeTuner::getDeltaSpace() const
		{
			return scheme->getDeltaSpace();
		}

		double DeltaTimeTuner::getDeltaTime() const
		{
			return bestDeltaTime;
		}

		std::vector<Norms> DeltaTimeTuner::getNorms() const
		{
			return bestNorms;
		}

		std::vector<std::vector<double>> DeltaTimeTuner::getAnalyticalValues() const
		{
			return analyticalValues;
		}

		std::vector<std::vector<double>> DeltaTimeTuner::getNumericalValues() const
		{
			return numericalValues;
		}
	}
}
