﻿///
/// \file InputParser.cpp
///
/// \author Bartłomiej Szostek
/// \date November 2016
///
/// \brief Program parameters reader class methods implementations.
//////////////////////////////////////////////////////////////////////////

#include "InputParser.h"

namespace advection
{
	namespace IO
	{
		InputParser::InputParser(int &argc, char *argv[])
		{
			// Skip first parameter - program name
			for (auto i = 1; i < argc; i++)
			{
				tokens.push_back(std::string(argv[i]));
			}
		}

		std::string InputParser::getOption(const std::string &option) const
		{
			auto iterator = std::find(tokens.begin(), tokens.end(), option);
			if (iterator != tokens.end() && ++iterator != tokens.end())
			{
				return *iterator;
			}
			return "";
		}

		bool InputParser::optionExists(const std::string &option) const
		{
			auto iterator = std::find(tokens.begin(), tokens.end(), option);
			return iterator != tokens.end();
		}
	}
}
