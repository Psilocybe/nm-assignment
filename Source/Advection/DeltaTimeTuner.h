﻿///
/// \file DeltaTimeTuner.h
///
/// \author Bartłomiej Szostek
/// \date October 2016
///
/// \brief Declaration of DeltaTimeTuner that looks for best params of scheme.
//////////////////////////////////////////////////////////////////////////

#ifndef _DELTA_TIME_TUNER_H
#define _DELTA_TIME_TUNER_H

#include "IScheme.h"

#include <vector>

namespace advection
{
	/// Encapsulates helper classes used in all parts of program.
	namespace tool
	{
		/// Manipulates delta time for scheme to find optimal solution.
		class DeltaTimeTuner
		{
			/// Scheme for which class will look for best parameter.
			scheme::IScheme* scheme;

			/// Number of tuning iterations.
			int iterations;
			
			/// Holds value of best 2 norm average currently found for scheme.
			double lowestTwoNormAvg;

			/// Delta time for which average of 2 norm was the lowest.
			double bestDeltaTime;

			/// Set of all calculated norms for best solution.
			std::vector<Norms> bestNorms;

			/// Vector of analytical values for best solution.
			std::vector<std::vector<double>> analyticalValues;

			/// Vector of numerical values for best solution.
			std::vector<std::vector<double>> numericalValues;
			
			/// Get average value of 2 norm.
			///
			/// @param errorNorms Vector of norms.
			/// @return Average of 2 norm.
			double getAverageTwoNorm(std::vector<Norms> errorNorms) const;

		public:
			/// Initializes all internal values of DeltaTimeTuner.
			///
			/// @param scheme Scheme to tune.
			/// @param iterations Number of tuning iterations.
			explicit DeltaTimeTuner(scheme::IScheme* scheme, int iterations);

			/// Launches tuning of delta time parameter.
			void tune();
			
			/// Get delta time for which average of 2 norm was the lowest.
			///
			/// @return Best delta time found.
			double getDeltaTime() const;

			/// Get delta space used in scheme.
			///
			/// @return Delta space of scheme.
			double getDeltaSpace() const; 

			/// Get set of best norms found.
			///
			/// @return Set of norms.
			std::vector<Norms> getNorms() const;

			/// Get vector of analytical values for best solution.
			/// Values are stored in 2D vector where first dimension corresponds time
			/// levels, and the second one holds values for each point in the grid
			///
			/// @return Vector of analytical values for best solution.
			std::vector<std::vector<double>> getAnalyticalValues() const;

			/// Get vector of numerical values for best solution.
			/// Values are stored in 2D vector where first dimension corresponds time
			/// levels, and the second one holds values for each point in the grid
			///
			/// @return Vector of numerical values for best solution.
			std::vector<std::vector<double>> getNumericalValues() const;
		};
	}
}

#endif
