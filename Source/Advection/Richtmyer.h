﻿///
/// \file Richtmyer.h
///
/// \author Bartłomiej Szostek
/// \date October 2016
///
/// \brief Declaration of advection solver based on Richtmyer scheme.
//////////////////////////////////////////////////////////////////////////

#ifndef _RICHTMYER_H
#define _RICHTMYER_H

#include "IScheme.h"

namespace advection
{
	namespace scheme
	{
		/// Advection equation solver implementing Richtmyer scheme.
		class Richtmyer :
			public IScheme
		{
			/// Calculates the correction value for time n + 1/2. 
			///
			/// @param previousStep Vector of previous values.
			/// @param i Point in space for which correction is calculated.
			/// @return Correction value.
			double halfTimeCorrection(const std::vector<double>& previousStep, const int i) const;

		protected:
			/// Checks if scheme is stable for given step in time and configuration.
			///
			/// @param deltaTime Step in time.
			/// @return True for stable scheme, false otherwise.
			bool isStable(double deltaTime) override;

			/// Calculates values for next time step.
			///
			/// @param previousStep Vector of values from previous step.
			/// @return Calculated vector.
			std::vector<double> calculateStep(std::vector<double> previousStep) override;

		public:
			/// Initializes scheme by configuration parameters.
			///
			/// @param configuration Configuration parameters for simulation.
			explicit Richtmyer(IO::AdvectionConfiguration &configuration);

			/// Get maximum value of step in space for which scheme is stable.
			///
			/// @return Maximum step in space for Richtmyer.
			double getMaxDeltaSpaceForStableScheme() const override;
		};
	}
}

#endif
