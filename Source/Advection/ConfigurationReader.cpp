﻿///
/// \file ConfigurationReader.cpp
///
/// \author Bartłomiej Szostek
/// \date November 2016
///
/// \brief Implementation of configuration reader.
//////////////////////////////////////////////////////////////////////////

#include "ConfigurationReader.h"

#include <fstream>
#include <algorithm>

namespace advection
{
	namespace IO
	{
		void ConfigurationReader::readConfiguration(std::istream &stream)
		{
			std::string line;
			while (std::getline(stream, line))
			{
				if (line[0] == commentCharacter) continue; //skip line of comment
				std::istringstream is_line(line);
				std::string key;
				if (std::getline(is_line, key, keyValueDelimiter))
				{
					std::string value;
					if (std::getline(is_line, value))
					{
						key = makeKey(key);
						if (options.count(key))
							throw std::logic_error("Duplicate configuration keys: " + key + ".");
						options.insert(std::make_pair(key, value));
					}
				}
			}
		}

		std::string ConfigurationReader::makeKey(std::string key) const
		{
			std::transform(key.begin(), key.end(), key.begin(), ::tolower);
			return key;
		}

		ConfigurationReader::ConfigurationReader(const std::string &configFileName)
		{
			std::ifstream configStream(configFileName);
			if (!configStream.is_open())
				throw std::runtime_error("Unable to open configuration file.");
			readConfiguration(configStream);
		}

		ConfigurationReader::ConfigurationReader(std::istream &stream)
		{
			readConfiguration(stream);
		}

		bool ConfigurationReader::parameterExists(const std::string& key) const
		{
			return options.count(makeKey(key)) > 0;
		}

		Parameter ConfigurationReader::getParameter(const std::string key) const
		{
			try
			{
				auto value = options.at(makeKey(key));
				return{ value };
			}
			catch (const std::out_of_range&)
			{
				throw std::out_of_range("Configuration key not found: " + key + ".");
			}
		}
	}
}
