﻿///
/// \file Functions.h
///
/// \author Bartłomiej Szostek
/// \date November 2016
///
/// \brief Analytical solutions for advection equation.
///
/// Functions that are solving analytically advection equation.
//////////////////////////////////////////////////////////////////////////

#ifndef _FUNCTIONS_H
#define _FUNCTIONS_H

#include <type_traits>

/// Global namespace common to all files.
namespace advection
{
	/// Encapsulates analytical solutions for advection equation.
	namespace analytical
	{
		/// Sign function for not signed type.
		/// Specialized template sign function for values of types, that does not have sign bit.
		///
		/// @param value Number to calculate sign for
		/// @param is_signed Flag indicating if type of value is signed
		/// @return Sign result of given value
		template <typename T> constexpr
			static int signum(T value, std::false_type is_signed)
		{
			return T(0) < value;
		}

		/// Sign function for signed type.
		/// Specialized template sign function for values of types, that does have sign bit.
		///
		/// @param value Number to calculate sign for
		/// @param is_signed Flag indicating if type of value is signed
		/// @return Sign result of given value
		template <typename T> constexpr
			static int signum(T value, std::true_type is_signed)
		{
			return (T(0) < value) - (value < T(0));
		}

		/// Universal type sign function.
		/// Templated sign function for values of any type.
		///
		/// @param value Number to calculate sign for
		/// @return Sign result of given value
		template <typename T> constexpr
			static int signum(T value)
		{
			return signum(value, std::is_signed<T>());
		}

		/// Analytical solution for advection equation based on sign function.
		/// Calculates value of function: \f$0.5 * (sgn(x - u*t) + 1)\f$
		///
		/// @param x Point in space.
		/// @param time Current time.
		/// @param acceleration Acceleration of wave.
		/// @return Value of sign based analytical solution.
		double signumFunction(const double x, const double time, const double acceleration);

		/// Analytical solution for advection equation based on exponential function.
		/// Calculates value of function: \f$0.5 * exp(-(x - u*t)^2)\f$
		///
		/// @param x Point in space.
		/// @param time Current time.
		/// @param acceleration Acceleration of wave.
		/// @return Value of exponential based analytical solution.
		double exponentialFunction(const double x, const double time, const double acceleration);
	}
}

#endif
