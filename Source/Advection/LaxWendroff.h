﻿///
/// \file LaxWendroff.h
///
/// \author Bartłomiej Szostek
/// \date October 2016
///
/// \brief Declaration of advection solver based on Lax-Wendroff scheme.
//////////////////////////////////////////////////////////////////////////

#ifndef _LAX_WENDROFF_H
#define _LAX_WENDROFF_H

#include "IScheme.h"

namespace advection
{
	namespace scheme
	{
		/// Advection equation solver implementing Lax-Wendroff scheme.
		class LaxWendroff :
			public IScheme
		{
		protected:
			/// Checks if scheme is stable for given step in time and configuration.
			///
			/// @param deltaTime Step in time.
			/// @return True for stable scheme, false otherwise.
			bool isStable(double deltaTime) override;

			/// Calculates values for next time step.
			///
			/// @param previousStep Vector of values from previous step.
			/// @return Calculated vector.
			std::vector<double> calculateStep(std::vector<double> previousStep) override;

		public:
			/// Initializes scheme by configuration parameters.
			///
			/// @param configuration Configuration parameters for simulation.
			explicit LaxWendroff(IO::AdvectionConfiguration &configuration);

			/// Get maximum value of step in space for which scheme is stable.
			///
			/// @return Maximum step in space for Lax-Wendroff.
			double getMaxDeltaSpaceForStableScheme() const override;
		};
	}
}

#endif
