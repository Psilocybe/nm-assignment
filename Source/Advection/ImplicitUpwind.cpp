///
/// \file ImplicitUpwind.cpp
///
/// \author Bartłomiej Szostek
/// \date October 2016
///
/// \brief Implementation of implicit upwind solver.
//////////////////////////////////////////////////////////////////////////

#include "ImplicitUpwind.h"

namespace advection
{
	namespace scheme
	{
		bool ImplicitUpwind::isStable(double deltaTime)
		{
			auto cfl = calculateCFL(deltaTime);
			return (cfl <= -1) || (0 <= cfl);
		}

		void ImplicitUpwind::forward(std::vector<double>& currentStep, const std::vector<double>& previousStep) const
		{
			for (auto g = 1; g < gridSize; g++)
			{
				currentStep[g] = (previousStep[g] + cfl * currentStep[g - 1]) / (1.0 + cfl);
			}
		}

		void ImplicitUpwind::backward(std::vector<double>& currentStep, const std::vector<double>& previousStep) const
		{
			for (auto g = gridSize - 1; g > 1; g--)
			{
				currentStep[g - 1] = ((1 + cfl) * currentStep[g] - previousStep[g]) / cfl;
			}
		}

		void ImplicitUpwind::afterSolving()
		{
			if (cfl < 0)
			{
				for (auto t = 0; t < timeLevels.size(); t++)
				{
					auto offset = ceil(-2.0 * acceleration * timeLevels[t] * (1.0 / deltaSpace));
					for (auto i = 0; i < offset; i++)
					{
						for (auto g = gridSize - 1; g > 0; g--)
						{
							numericalValues[t][g] = numericalValues[t][g - 1];
						}
						for (auto g = gridSize - 1; g > 0; g--)
						{
							analyticalValues[t][g] = analyticalValues[t][g - 1];
						}
					}
				}
			}
		}

		std::vector<double> ImplicitUpwind::calculateStep(std::vector<double> previousStep)
		{
			std::vector<double> currentStep(gridSize);
			currentStep[0] = lowerBoundaryValue;
			currentStep[gridSize - 1] = upperBoundaryValue;
			if (cfl > 0)
			{
				forward(currentStep, previousStep);
			}
			else
			{
				backward(currentStep, previousStep);
			}
			return currentStep;
		}

		ImplicitUpwind::ImplicitUpwind(IO::AdvectionConfiguration& configuration)
			: IScheme(configuration)
		{
		}

		double ImplicitUpwind::getMaxDeltaSpaceForStableScheme() const
		{
			// This scheme is unconditionally stable, so return big time step
			return 1.0;
		}
	}
}
