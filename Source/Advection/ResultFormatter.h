﻿///
/// \file ResultFormatter.h
///
/// \author Bartłomiej Szostek
/// \date November 2016
///
/// \brief Declaration of result formatter based on IFormatter.
//////////////////////////////////////////////////////////////////////////

#ifndef _RESULT_FORMATTER_H
#define _RESULT_FORMATTER_H

#include "IFormatter.h"

#include <vector>

namespace advection
{
	namespace IO
	{
		/// Saves formated result of simulation to output file.
		class ResultFormatter
			: public IFormatter
		{
			/// Delimiter used to separate values in columns.
			const char delimiter = '\t';

			/// Number of significant digits of values saved to file.
			const int resultPrecision = 6;

			/// Vector of discretized grid values used in simulation.
			std::vector<double> grid;

			/// Vector of time levels produced in simulation.
			std::vector<double> timeLevels;

			/// Vector of numerical values produced in simulation for each time level.
			std::vector<std::vector<double>> numericalValues;

			/// Vector of analytical values produced in simulation for each time level.
			std::vector<std::vector<double>> analyticalValues;

			/// Makes row of data of given position.
			///
			/// @param position Position in data vectors.
			void saveValues(int position);

		public:
			/// Initializes result formatters internal values.
			///
			/// @param grid Vector of discretized grid values used in simulation.
			/// @param analyticalValues Vector of analytical values for each time level.
			/// @param numericalValues Vector of numerical values for each time level.
			/// @param timeLevels Vector of time levels produced in simulation.
			/// @param fileName Output result file name.
			ResultFormatter(
				std::vector<double> grid,
				std::vector<std::vector<double>> analyticalValues,
				std::vector<std::vector<double>> numericalValues,
				std::vector<double> timeLevels,
				const std::string& fileName);

			/// Formats and saves result to output file.
			/// Overwritten method from IFormatter
			void save() override;
		};
	}
}

#endif
