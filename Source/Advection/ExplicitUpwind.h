﻿///
/// \file ExplicitUpwind.h
///
/// \author Bartłomiej Szostek
/// \date October 2016
///
/// \brief Declaration of advection solver based on explicit upwind scheme.
//////////////////////////////////////////////////////////////////////////

#ifndef _EXPLICIT_UPWIND_H
#define _EXPLICIT_UPWIND_H

#include "IScheme.h"

namespace advection
{
	namespace scheme
	{
		/// Advection equation solver implementing explicit upwind scheme.
		class ExplicitUpwind
			: public IScheme
		{
		protected:
			/// Checks if scheme is stable for given step in time and configuration.
			///
			/// @param deltaTime Step in time.
			/// @return True for stable scheme, false otherwise.
			bool isStable(double deltaTime) override;

			/// Calculates values for next time step.
			///
			/// @param previousStep Vector of values from previous step.
			/// @return Calculated vector.
			std::vector<double> calculateStep(std::vector<double> previousStep) override;

		public:
			/// Initializes scheme by configuration parameters.
			///
			/// @param configuration Configuration parameters for simulation.
			explicit ExplicitUpwind(IO::AdvectionConfiguration &configuration);

			/// Get maximum value of step in space for which scheme is stable.
			///
			/// @return Maximum step in space for explicit upwind.
			double getMaxDeltaSpaceForStableScheme() const override;
		};
	}
}

#endif
