﻿///
/// \file Richtmyer.cpp
///
/// \author Bartłomiej Szostek
/// \date October 2016
///
/// \brief Implementation of Richtmyer two-step solver.
//////////////////////////////////////////////////////////////////////////

#include "Richtmyer.h"

namespace advection
{
	namespace scheme
	{
		double Richtmyer::halfTimeCorrection(const std::vector<double>& previousStep, const int i) const
		{
			return 0.5 * (previousStep[i + 1] + previousStep[i - 1]) - cfl / 4 * (previousStep[i + 1] - previousStep[i - 1]);
		}

		bool Richtmyer::isStable(double deltaTime)
		{
			return calculateCFL(deltaTime) <= 2.0;
		}

		std::vector<double> Richtmyer::calculateStep(std::vector<double> previousStep)
		{
			std::vector<double> currentStep(gridSize);
			currentStep[0] = lowerBoundaryValue;
			currentStep[1] = previousStep[1];
			currentStep[gridSize - 2] = previousStep[gridSize - 2];
			currentStep[gridSize - 1] = upperBoundaryValue;
			for (auto g = 2; g < gridSize - 2; g++)
			{
				auto u_mid_prev = halfTimeCorrection(previousStep, g - 1);
				auto u_mid_next = halfTimeCorrection(previousStep, g + 1);
				currentStep[g] = previousStep[g] - cfl / 2 * (u_mid_next - u_mid_prev);
			}
			return currentStep;
		}

		Richtmyer::Richtmyer(IO::AdvectionConfiguration& configuration)
			: IScheme(configuration)
		{
		}

		double Richtmyer::getMaxDeltaSpaceForStableScheme() const
		{
			auto maxDeltaTime = 2 * deltaSpace / acceleration;
			return maxDeltaTime;
		}
	}
}
