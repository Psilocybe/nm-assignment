﻿///
/// \file MetadataFormatter.h
///
/// \author Bartłomiej Szostek
/// \date November 2016
///
/// \brief Declaration of metadata formatter based on IFormatter.
//////////////////////////////////////////////////////////////////////////

#ifndef _METADATA_FORMATTER_H
#define _METADATA_FORMATTER_H

#include "AdvectionConfiguration.h"
#include "IFormatter.h"
#include "Norms.h"

namespace advection
{
	namespace IO
	{
		/// Saves formated metadata of simulation to output file.
		class MetadataFormatter :
			public IFormatter
		{
			/// Name of scheme used in simulation.
			std::string scheme;
			
			/// Grid size of simulation.
			size_t gridSize;

			/// Delta time of simulation.
			double deltaTime; 

			/// Delta space of simulation.
			double deltaSpace;

			/// Vector of error norms produced in simulation.
			std::vector<tool::Norms> norms;
			
		public:
			/// Initializes metadata formatters internal values.
			///
			/// @param gridSize Grid size of simulation.
			/// @param deltaTime Delta time of simulation.
			/// @param deltaSpace Delta space of simulation.
			/// @param norms Vector of error norms produced in simulation.
			/// @param scheme Name of scheme.
			/// @param fileName Output result file name.
			explicit MetadataFormatter(
				size_t gridSize,
				double deltaTime,
				double deltaSpace,
				std::vector<tool::Norms> norms,
				std::string scheme,
				std::string& fileName);

			/// Formats and saves metadata to output file.
			/// Overwritten method from IFormatter
			void save() override;
		};
	}
}

#endif
