﻿///
/// \mainpage Linear Advection Equation - Numerical Solution
///
/// \section intro_sec Introduction
/// This program solves numerically a linear advection equation, by using
/// numerical schemes discussed in the Computational Methods lectures at
/// Cranfield University, UK. Current implementation supports Explicit and
/// Implicit Upwind scheme, Richtmyer and Lax-Wendroff methods. 
///
/// \section usage_sec Usage
/// Program takes as parameters following things:
/// - \-config [config_path]
///		Path to configuration file with simulation arguments
/// - \-out [out_file_name]
///		Name of the output file where the results will be saved
///
/// \section config_sec Configuration file
///	Configuration file must contain all of the below parameters.
/// Line starting with `#` is a comment and will be ignored by program.
/// Keys and values should be separated by `=`.
///    <CODE>
///    \n\# Example config files, order of parameters is not important
///    \n\# [int] Number of points into which program will divide space 
///    \n gridSize=100
///    \n\# [double] Lower boundary of space domain
///    \n lowerBoundary=-50.0
///    \n\# [double] Upper boundary of space domain
///    \n upperBoundary=50.0
///    \n\# [double] Value of function in lower boundary point
///    \n lowerBoundaryValue=0.0
///    \n\# [double] Value of function in upper boundary point
///    \n upperBoundaryValue=0.0
///    \n\# [double] Acceleration parameter for advection equation
///    \n acceleration=1.75
///    \n\# [string] Scheme name, available:
///    \n\# explicit-upwind, implicit-upwind, richtmyer, lax-wendroff
///    \n scheme=explicit-upwind
///    \n\# [string] Name of initialization function, available: 
///    \n\# signum, exponential
///    \n initFunction=exponential
///    \n\# [arr of int] Time levels which will be saved as result of program
///    \n timeLevels=0 5 10
///    \n\# [int] tune {opt} Number of iterations for DeltaTimeTuner. If
///    \n\# this parameter is specified, program will search for optimal
///    \n\# delta(t) for given arguments
///    \n tune=100
///	   \n\# [double] {opt} Delta time for which user want to produce results
///	   \n deltaTime=0.1
///    </CODE>
///
/// One from the `tune` or `deltaTime` parameters have to be specified.
/// \section results_sec Results
///		Results of the program are saved into two separate files.
///		First of them is the result itself, that contains data stored in
///		columns:
///		<CENTER>
///		X Analytical(T=t1) Numerical(T=t1) ... Analytical(T=tN) Numerical(T=tN)
///		</CENTER>
///
///		The second file stores the metadata, that is related to the
///     produced results, such as error norms and parameters values.
//////////////////////////////////////////////////////////////////////////
///
/// \file Main.cpp
///
/// \author Bartłomiej Szostek
/// \date November 2016
///
/// \brief Entry point of application
///
/// Application's main function.
//////////////////////////////////////////////////////////////////////////

#include "AdvectionConfiguration.h"
#include "ConfigurationReader.h"
#include "DeltaTimeTuner.h"
#include "ResultFormatter.h"
#include "InputParser.h"
#include "IScheme.h"
#include "MetadataFormatter.h"

#include <iomanip>
#include <iostream>

//////////////////////////////////////////////////////////////////////////
/// Application's main function.
/// This function describes program flow and is responsible for exception
/// handling and reporting any errors to user.
///
/// @param argc Argument count
/// @param argv Argument vector
/// @return Exit code
int main(int argc, char* argv[])
{
	try
	{
		using namespace advection;

		// Get command line arguments
		IO::InputParser parser(argc, argv);
		const auto configPath = parser.getOption("-config");
		const auto outputFileName = parser.getOption("-out");

		// Create configuration for advection solver
		IO::ConfigurationReader reader(configPath);
		std::string schemeName = reader.getParameter("scheme");
		auto configuration = readConfig(reader);

		// Solve problem with chosen scheme
		auto scheme = scheme::IScheme::makeScheme(schemeName, configuration);

		std::vector<std::vector<double>> analyticalValues;
		std::vector<std::vector<double>> numericalValues;
		std::vector<tool::Norms> norms;
		double deltaTime;
		double deltaSpace;

		// If tune option is present tune delta time parameter
		if (reader.parameterExists("tune"))
		{
			int iterations = reader.getParameter("tune");
			tool::DeltaTimeTuner tuner(scheme, iterations);
			tuner.tune();
			analyticalValues = tuner.getAnalyticalValues();
			numericalValues = tuner.getNumericalValues();
			norms = tuner.getNorms();
			deltaTime = tuner.getDeltaTime();
			deltaSpace = tuner.getDeltaSpace();
		}
		// If deltaTime option is present calculate solution for only one value
		else if(reader.parameterExists("deltaTime"))
		{
			double dT = reader.getParameter("deltaTime");
			scheme->solve(dT);
			analyticalValues = scheme->getAnalyticalValues();
			numericalValues = scheme->getNumericalValues();
			norms = scheme->getNorms();
			deltaTime = dT;
			deltaSpace = scheme->getDeltaSpace();
		}
		else
		{
			throw std::runtime_error("Missing 'tune' or 'deltaTime' parameters in configuration.");
		}

		// Save results
		IO::ResultFormatter results(
			scheme->getGrid(),
			analyticalValues,
			numericalValues,
			configuration.timeLevels,
			outputFileName);
		results.save();
	
		// Save metadata
		auto metadataFileName = outputFileName.substr(0, outputFileName.find_last_of(".")) + ".meta";
		IO::MetadataFormatter metadata(
			configuration.gridSize,
			deltaTime,
			deltaSpace,
			norms,
			schemeName,
			metadataFileName);
		metadata.save();

		// Cleanup and return with normal exit code
		delete scheme;
		return 0;
	}
	catch (std::logic_error& err)
	{
		std::cerr << "Logic error: " << typeid(err).name() << std::endl;
		std::cerr << err.what() << std::endl;
		return -1;
	}
	catch (std::runtime_error& err)
	{
		std::cerr << "Runtime error: " << typeid(err).name() << std::endl;
		std::cerr << err.what() << std::endl;
		return -2;
	}
	catch (std::exception& err)
	{
		std::cerr << "Unknown error: " << typeid(err).name() << std::endl;
		std::cerr << err.what() << std::endl;
		return -3;
	}
}
