﻿///
/// \file LaxWendroff.cpp
///
/// \author Bartłomiej Szostek
/// \date October 2016
///
/// \brief Implementation of Lax-Wendroff solver.
//////////////////////////////////////////////////////////////////////////

#include "LaxWendroff.h"

namespace advection
{
	namespace scheme
	{
		bool LaxWendroff::isStable(double deltaTime)
		{
			return calculateCFL(deltaTime) <= 1.0;
		}

		std::vector<double> LaxWendroff::calculateStep(std::vector<double> previousStep)
		{
			std::vector<double> currentStep(gridSize);
			currentStep[0] = lowerBoundaryValue;
			currentStep[gridSize - 1] = upperBoundaryValue;
			for (auto g = 1; g < gridSize - 1; g++)
			{
				auto spaceCorrection = 0.5 * cfl * cfl * (previousStep[g + 1] - 2 * previousStep[g] + previousStep[g - 1]);
				currentStep[g] = previousStep[g] - cfl / 2 * (previousStep[g + 1] - previousStep[g - 1]) + spaceCorrection;
			}
			return currentStep;
		}

		LaxWendroff::LaxWendroff(IO::AdvectionConfiguration& configuration)
			: IScheme(configuration)
		{
		}

		double LaxWendroff::getMaxDeltaSpaceForStableScheme() const
		{
			auto maxDeltaTime = deltaSpace / acceleration;
			return maxDeltaTime;
		}
	}
}
