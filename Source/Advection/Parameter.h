﻿///
/// \file Parameter.h
///
/// \author Bartłomiej Szostek
/// \date November 2016
///
/// \brief Parameter template for easier parsing of configuration files.
//////////////////////////////////////////////////////////////////////////

#include <vector>
#include <sstream>

namespace advection
{
	namespace IO
	{
		/// @brief Structure that allows implicit casting strings to other types without.
		///
		/// This class uses templated overloads of constructors and string streams
		/// for casting values read from configurations to easily convert them to
		/// fields in configuration structure.
		///
		/// @see ConfigurationReader::lookupParameter
		struct Parameter
		{
			/// String read from configuration.
			std::string value;

			/// Template of conversion method for basic types.
			///
			/// @param ptr Pointer of type T.
			/// @return Value of parameter casted to type T.
			/// @throws runtime_error When parameter cannot be converted.
			template<typename T>
			T convert(T* ptr) const
			{
				std::stringstream ss(value);
				T convertedValue;
				if (ss >> convertedValue)
				{
					return convertedValue;
				}
				throw std::runtime_error("Parameter conversion failed.");
			}

			/// Template of conversion method for vector types of type T.
			///
			/// @param ptr Pointer to std::vector of type T.
			/// @return Vector containing converted values.
			template<typename T>
			std::vector<T> convert(std::vector<T> * ptr) const
			{
				std::stringstream ss(value);
				T convertedValue;
				std::vector<T> values;
				while (ss >> convertedValue)
				{
					values.push_back(convertedValue);
				}
				return values;
			}

			/// Overloaded template of constructor that uses convert methods. 
			template<typename T>
			operator T() const
			{
				// Cast type T to its pointer to determine which convert method should be used
				return this->convert(static_cast<T*>(nullptr));
			}
		};
	}
}
