﻿///
/// \file IScheme.cpp
///
/// \author Bartłomiej Szostek
/// \date October 2016
///
/// \brief Implementation of non-abstract methods and scheme factory.
//////////////////////////////////////////////////////////////////////////

#include "ExplicitUpwind.h"
#include "ImplicitUpwind.h"
#include "LaxWendroff.h"
#include "Richtmyer.h"

#include <cmath>
#include <functional>
#include <stdexcept>

namespace advection
{
	namespace scheme
	{
		// FACTORY
		//////////////////////////////////////////////////////////////////////////

		IScheme* IScheme::makeScheme(std::string schemeName, IO::AdvectionConfiguration& configuration)
		{
			if (schemeName == "explicit-upwind")
				return new ExplicitUpwind(configuration);
			if (schemeName == "implicit-upwind")
				return new ImplicitUpwind(configuration);
			if (schemeName == "richtmyer")
				return new Richtmyer(configuration);
			if (schemeName == "lax-wendroff")
				return new LaxWendroff(configuration);
			throw std::invalid_argument("Unknown scheme requested.");
		}

		void IScheme::solve(double deltaTime)
		{
			if (!isStable(deltaTime))
				throw std::domain_error("Scheme is unstable for given delta(time) = " + std::to_string(deltaTime));
			prepareContainers();
			auto step = initialize(deltaTime);
			for (size_t tl = 0; tl < timeLevels.size(); tl++)
			{
				while (currentTime <= timeLevels[tl])
				{
					currentTime += deltaTime;
					step = calculateStep(step);
				}
				numericalValues[tl] = step;
				calculateError(tl);
			}
			afterSolving();
		}

		// INTERNAL
		//////////////////////////////////////////////////////////////////////////

		void IScheme::calculateError(int timeStep)
		{
			std::vector<double> error(gridSize);
			for (auto i = 0; i < gridSize; i++)
			{
				auto analytical = solveAnalytically(grid[i], currentTime, acceleration);
				analyticalValues[timeStep].push_back(analytical);
				error[i] = analytical - numericalValues[timeStep][i];
			}
			auto norms = tool::calculateNorms(error);
			errorNorms.push_back(norms);
		}

		void IScheme::prepareContainers()
		{
			errorNorms = std::vector<tool::Norms>();
			numericalValues = std::vector<std::vector<double>>(timeLevels.size());
			analyticalValues = std::vector<std::vector<double>>(timeLevels.size());
		}

		double IScheme::calculateCFL(double deltaTime) const
		{
			return (acceleration * deltaTime) / deltaSpace;
		}

		std::vector<double> IScheme::initialize(double deltaTime)
		{
			std::vector<double> initialStep(gridSize);
			cfl = calculateCFL(deltaTime);
			currentTime = 0.0;
			initialStep[0] = lowerBoundaryValue;
			for (auto i = 1; i < gridSize - 1; i++)
			{
				initialStep[i] = solveAnalytically(grid[i], currentTime, acceleration);
			}
			initialStep[gridSize - 1] = upperBoundaryValue;
			return initialStep;
		}

		// CONSTRUCTORS
		//////////////////////////////////////////////////////////////////////////

		IScheme::IScheme(IO::AdvectionConfiguration& configuration)
		{
			currentTime = 0.0;
			gridSize = configuration.gridSize;
			acceleration = configuration.acceleration;
			lowerBoundary = configuration.lowerBoundary;
			upperBoundary = configuration.upperBoundary;
			lowerBoundaryValue = configuration.lowerBoundaryValue;
			upperBoundaryValue = configuration.upperBoundaryValue;
			solveAnalytically = configuration.initFunction;
			timeLevels = configuration.timeLevels;
			deltaSpace = (upperBoundary - lowerBoundary) / (gridSize - 1);
			auto x = lowerBoundary;
			for (auto i = 0; i < gridSize; i++, x += deltaSpace)
				grid.push_back(x);
			numericalValues = std::vector<std::vector<double>>(timeLevels.size());
			analyticalValues = std::vector<std::vector<double>>(timeLevels.size());
		}

		IScheme::~IScheme()
		{
		}

		// ACCESSOR METHODS
		//////////////////////////////////////////////////////////////////////////

		double IScheme::getDeltaSpace() const
		{
			return deltaSpace;
		}

		std::vector<double> IScheme::getGrid() const
		{
			return grid;
		}

		std::vector<std::vector<double>> IScheme::getAnalyticalValues() const
		{
			return analyticalValues;
		}

		std::vector<tool::Norms> IScheme::getNorms() const
		{
			return errorNorms;
		}

		std::vector<std::vector<double>> IScheme::getNumericalValues() const
		{
			return numericalValues;
		}
	}
}
