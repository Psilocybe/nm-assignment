﻿///
/// \file IFormatter.cpp
///
/// \author Bartłomiej Szostek
/// \date November 2016
///
/// \brief Implementation of non abstract output formatter methods.
//////////////////////////////////////////////////////////////////////////

#include "IFormatter.h"

namespace advection
{
	namespace IO
	{
		IFormatter::IFormatter(std::string fileName)
			: outputFile(fileName)
		{
			outputFile.open(fileName, std::ios_base::out);
		}

		IFormatter::~IFormatter()
		{
			outputFile.close();
		}
	}
}
