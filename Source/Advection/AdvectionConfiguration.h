﻿///
/// \file AdvectionConfiguration.h
///
/// \author Bartłomiej Szostek
/// \date November 2016
///
/// \brief Configuration of advection equation and configuration parser.
//////////////////////////////////////////////////////////////////////////

#ifndef _ADVECTION_CONFIGURATION_H
#define _ADVECTION_CONFIGURATION_H

#include "ConfigurationReader.h"

#include <vector>

namespace advection
{
	/// Encapsulates code responsible for Input/Output operations.
	namespace IO
	{
		/// Contains configuration for advection solver.
		struct AdvectionConfiguration
		{
			size_t gridSize; /*!< Size of the discretized grid. */ 
			double lowerBoundary; /*!< Lower boundary of space domain. */ 
			double upperBoundary; /*!< Upper boundary of space domain. */
			double lowerBoundaryValue; /*!< Value of initial wave at lower boundary. */
			double upperBoundaryValue; /*!< Value of initial wave at upper boundary. */
			double acceleration; /*!< Acceleration of wave. */
			double(*initFunction)(double x, double time, double acceleration); /*!< Initialization function pointer. */
			std::vector<double> timeLevels; /*!< Time points at which solver will calculate error and save values. */
		};

		/// Writes parameters from ConfigurationReader to AdvectionConfiguration.
		/// Each of parameter is read by its name and stored inside corresponding
		/// structure field.
		///
		/// @param reader Reference to object which stores all params read from file.
		/// @return Filed configuration structure.
		/// @throws invalid_argument When parameter cannot be assigned or its value is invalid.
		AdvectionConfiguration readConfig(ConfigurationReader &reader);
	}
}

#endif
