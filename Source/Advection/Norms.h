﻿///
/// \file Norms.h
///
/// \author Bartłomiej Szostek
/// \date October 2016
///
/// \brief Contains tools to calculate norms of vectors.
//////////////////////////////////////////////////////////////////////////

#ifndef _NORMS_H
#define _NORMS_H

#include <cmath>
#include <vector>
#include <numeric>

namespace advection
{
	namespace tool
	{
		/// Keeps information about all norms in one place.
		struct Norms
		{
			double one; /*!< Value of 1 norm. */
			double two; /*!< Value of 2 norm. */
			double uniform; /*!< Value of uniform (infinity) norm. */
		};

		/// Calculates 1 norm of vector.
		///
		/// @param vec Standard vector to calculate his 1 norm.
		/// @return Norm 1 of given vector.
		template<typename T>
		double oneNorm(const std::vector<T> vec)
		{
			auto oneNorm = std::accumulate(vec.begin(), vec.end(), 0.0,
				[](double sum, const double elem) { return sum + fabs(elem); });
			return oneNorm;
		}

		/// Calculates 2 norm of vector.
		///
		/// @param vec Standard vector to calculate his 2 norm.
		/// @return Norm 2 of given vector.
		template<typename T>
		double twoNorm(const std::vector<T> vec)
		{
			auto oneNorm = std::accumulate(vec.begin(), vec.end(), 0.0,
				[](double sum, const double elem) { return sum + elem * elem; });
			return sqrt(oneNorm);
		}

		/// Calculates uniform (infinity) norm of vector.
		///
		/// @param vec Standard vector to calculate his uniform norm.
		/// @return Uniform norm of given vector.
		template<typename T>
		double uniformNorm(const std::vector<T> vec)
		{
			auto max = fabs(vec.at(0));
			for (auto iter = vec.begin(); iter < vec.end(); ++iter)
			{
				auto current = fabs(*iter);
				if (max < current)
					max = current;
			}
			return max;
		}

		/// Calculate all norms for given vector.
		///
		/// @param vec Standard vector to calculate his norms.
		/// @return Structure containing all calculated norms.
		/// @throws logic_error When vector size is equal to 0.
		template<typename T>
		Norms calculateNorms(const std::vector<T> vec)
		{
			if (vec.size() == 0)
				throw std::logic_error("Cannot calculate norm on zero vector.");
			return Norms
			{
				oneNorm(vec),
				twoNorm(vec),
				uniformNorm(vec)
			};
		}
	}
}

#endif
