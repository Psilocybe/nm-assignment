﻿///
/// \file InputParser.h
///
/// \author Bartłomiej Szostek
/// \date November 2016
///
/// \brief Program parameters reader class declaration.
//////////////////////////////////////////////////////////////////////////

#ifndef _INPUT_PARSER_H
#define _INPUT_PARSER_H

#include <vector>

namespace advection
{
	namespace IO
	{
		/// @brief Helper class for dealing with program parameters.
		///
		/// It provides functionalities to check if parameter exists and
		/// retrieve it's value.
		class InputParser
		{
			/// Container for loaded program parameters.
			std::vector <std::string> tokens;

		public:
			/// Base constructor.
			/// Parses tokens passed to program through command line.
			///
			/// @param argc Arguments count. Size of the argv array.
			/// @param argv Arguments values stored in array.
			InputParser(int &argc, char *argv[]);


			/// Retrieves value of option by token.
			///
			/// @param option Token to find.
			/// @return If found return string value of option, empty string otherwise.
			std::string getOption(const std::string &option) const;

			/// Checks if specific token is present among parameters.
			///
			/// @param option Token to find.
			/// @return True if token was found, false otherwise.
			bool optionExists(const std::string &option) const;
		};
	}
}

#endif
