﻿///
/// \file IScheme.h
///
/// \author Bartłomiej Szostek
/// \date October 2016
///
/// \brief Declaration of abstract base class for all schemes and factory.
//////////////////////////////////////////////////////////////////////////

#ifndef _I_SCHEME_H
#define _I_SCHEME_H

#include "AdvectionConfiguration.h"
#include "Norms.h"

#include <vector>

namespace advection
{
	/// Encapsulates advection equation solvers.
	namespace scheme
	{
		/// @brief Base class for all schemes and factory of schemes.
		/// 
		/// IScheme is responsible for creation of schemes based on its name.
		/// This class is also base class for all schemes, it handles 
		/// initialization, error calculation by means of analytical solution.
		class IScheme
		{
			/// Container for calculated norms of error vector for each time level.
			std::vector<tool::Norms> errorNorms;

			/// Calculates error norms at specified time step.
			/// Norms are saved in norm container.
			///
			/// @param timeLevel Time level at which error will be calculated.
			void calculateError(int timeLevel);

			/// Resizes and zeros all containers for better performance.
			void prepareContainers();

		protected:
			/// Container for calculated numerical values.
			std::vector<std::vector<double>> numericalValues;

			/// Container for calculated analytical values.
			std::vector<std::vector<double>> analyticalValues;

			/// Courant–Friedrichs–Lewy number.
			double cfl;

			/// Discretized grid size.
			int gridSize;

			/// Lower domain point.
			double lowerBoundary;

			/// Upper domain point.
			double upperBoundary;

			/// Value of function at lower boundary.
			double lowerBoundaryValue;
			
			/// Value of function at upper boundary.
			double upperBoundaryValue;

			/// Step in space.
			double deltaSpace;

			/// Current time of simulation.
			double currentTime;

			/// Acceleration parameter of advection equation.
			double acceleration;

			/// Pointer to initialization function.
			double(*solveAnalytically) (double x, double time, double acceleration);
			
			/// Exact points in discretized domain.
			std::vector<double> grid;

			/// Vector of time levels, at which simulation saves values.
			std::vector<double> timeLevels;
			
			/// Calculates Courant–Friedrichs–Lewy number for given step in time.
			///
			/// @param deltaTime Step in time.
			/// @return Courant number.
			double calculateCFL(double deltaTime) const;

			/// Calculates initial values with given delta time.
			///
			/// @param deltaTime Step in time for simulation.
			/// @return Values calculated by initial function and boundary conditions.
			virtual std::vector<double> initialize(double deltaTime);

			/// Checks if scheme is stable for given step in time and configuration.
			///
			/// @param deltaTime Step in time.
			/// @return True for stable scheme, false otherwise.
			virtual bool isStable(double deltaTime) = 0;

			/// Calculates values for next time step.
			///
			/// @param previousStep Vector of values from previous step.
			/// @return Calculated vector.
			virtual std::vector<double> calculateStep(std::vector<double> previousStep) = 0;

			/// Invoked after end of simulation.
			/// This method is intentionally empty and exisits in case if some schemes
			/// require further actions like correction in the end of algorithm.
			virtual void afterSolving() {};

		public:
			// FACTORY
			//////////////////////////////////////////////////////////////////////////

			/// Scheme factory method.
			/// Returns initialized scheme by its name.
			///
			/// @param schemeName Name of the scheme.
			/// @param configuration Configuration structure to initialize scheme.
			/// @return Initialized scheme for simulation.
			/// @throws invalid_argument When scheme name is not recognized.
			static IScheme* makeScheme(std::string schemeName, IO::AdvectionConfiguration& configuration);

			/// Launches scheme solver.
			/// Until the current time reaches highest time level, algorithm will
			/// continue to calculate next time step values and increase time by
			/// provided delta time.
			///
			/// @param deltaTime Step in time.
			void solve(double deltaTime);

			// CONSTRUCTORS
			//////////////////////////////////////////////////////////////////////////

			/// Initializes scheme by configuration parameters.
			///
			/// @param configuration Configuration parameters for simulation.
			explicit IScheme(IO::AdvectionConfiguration& configuration);
			
			/// Virtual destructor for proper cleaning up.
			virtual ~IScheme();
			
			// ACCESSOR METHODS
			//////////////////////////////////////////////////////////////////////////

			/// Get step in space for grid in this simulation.
			///
			/// @return Step in space.
			double getDeltaSpace() const;

			/// Get discretized domain values.
			///
			/// @return Discretized grid.
			std::vector<double> getGrid() const;

			/// Get calculated numerical values for each time level.
			///
			/// @return Numerical values.
			std::vector<std::vector<double>> getNumericalValues() const;

			/// Get calculated analytical values for each time level.
			///
			/// @return Analytical values.
			std::vector<std::vector<double>> getAnalyticalValues() const;

			/// Get error norms of error vector for each time level.
			///
			/// @return Error norms.
			std::vector<tool::Norms> getNorms() const;

			/// Get maximum value of step in space for which scheme is stable.
			///
			/// @return Maximum step in space.
			virtual double getMaxDeltaSpaceForStableScheme() const = 0;
		};
	}
}

#endif
