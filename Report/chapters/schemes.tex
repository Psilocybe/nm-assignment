\chapter{Brief schemes analysis}
\label{ch:schemes}
	In this part of the report we present various ways of solving hyperbolic PDEs by utilizing finite difference approximations. Each section provides formula of the scheme, its stability conditions and order. Von Neumann stability analysis (a.k.a. Fourier stability analysis) is described in depth in appendix \ref{ch:stability}.
	
	To simplify equations, we introduce the dimensionless number called \emph{CFL number} according to Moin \cite{moin_2010}, named after the mathematicians Courant, Friedrich, and Lewy, equal to
	
	\begin{equation}\label{eq:courant}
		C=u\frac{\Delta t}{\Delta x}
	\end{equation}
	
	In terms of conditions provided for this assignment, the only unknown in equation \ref{eq:courant} is $\Delta t$. $\Delta x$ can be calculated using given domain boundaries and number of points in discretized grid, constant acceleration $u$ is given. 
	
	CFL condition is a necessary, but not sufficient condition that has to be met while solving hyperbolic PDEs numerically using discussed methods. In our one-dimensional advection problem, it has the following form $C \le C_{max}$, where $C_{max}$ varies between different schemes.
	
	To show dependencies between previously calculated points and currently calculating one, we will use geometrical arrangements of points called \emph{stencils}. In this paper we will use green nodes with crosshatch pattern to indicate currently calculated point, whereas blue nodes with diagonal lines are those, which scheme takes into account for calculations. Gray dotted nodes are just for preserving the grid. Each row of nodes indicates different time step or correction step, described on the left side.
	
	\section{Explicit Upwind}
		First-order upwind scheme is the simplest possible scheme to solve hyperbolic PDEs. It is given by formula as in Hoffman and Chiang \cite{hoffmann_2000}
		
		\begin{align}
			\label{eq:explicitupwind}
			\begin{split}
				\frac{f_{i}^{n+1} - f_{i}^{n}}{\Delta t} + u\frac{f_{i}^{n} - f_{i-1}^{n}}{\Delta x} = 0
				\\
				f_{i}^{n+1} = f_{i}^{n} - C(f_{i}^{n}-f_{i-1}^{n})
			\end{split}
		\end{align}
		
		As shown in equation \ref{eq:explicitupwind}, this scheme uses backward differencing in space and forward differencing in time. Figure \ref{fig:explicit-stencil} presents graphical representation of upwind scheme. We can see on which points currently calculated depends on. Explicit upwind scheme is first order accurate in time and space $\mathcal{O}(\Delta t, \Delta x)$.
		
		\begin{figure}
			\centering
			\begin{tikzpicture}
				\draw[dashed] (-4,1.2) -- (4,1.2);
				\node[black] at (-4.5,0.5) {$n+1$};
				\draw[dashed] (-4,-0.2) -- (4,-0.2);
				\node[black] at (-4.5,-0.9) {$n$};
				\draw[dashed] (-4,-1.6) -- (4,-1.6);
				
				\node[black] at (-1.5,-2) {$i-1$};
				\node[black] at (0,-2) {$i$};
				\node[black] at (+1.5,-2) {$i+1$};
				
				\stencilpt[gray,pattern=dots, pattern color=gray]{-1.5,0.5}{i-1}{};
				\stencilpt[green,pattern=crosshatch, pattern color=green]{0,0.5}{i}{};
				\stencilpt[gray,pattern=dots, pattern color=gray]{+1.5,0.5}{i+1}{};
				\stencilpt[pattern=north west lines, pattern color=blue]{-1.5,-0.9}{j-1}{};
				\stencilpt[pattern=north west lines, pattern color=blue]{0,-0.9}{j}{};
				\stencilpt[gray,pattern=dots, pattern color=gray]{+1.5,-0.9}{j+1}{};

				\draw[line width=0.5mm,->,>=stealth] (j-1)--(i);
				\draw[line width=0.5mm,->,>=stealth] (j)--(i);
			\end{tikzpicture}
			\caption{Stencil for explicit upwind scheme.}
			\label{fig:explicit-stencil}
		\end{figure}
	
	As shown in Collis \cite{collis_2005} and appendix \ref{ch:stability}, by using Von Neumann stability analysis, this method is conditionally stable. The CFL condition is as follows: $C \le C_{max} = 1.0$.
	
	\section{Implicit Upwind}
		Using first-order backward difference both in time and space leads to \ref{eq:implicitupwind}. This method is also first order accurate in time and space $\mathcal{O}(\Delta t, \Delta x)$.
		 
		\begin{align}
			\label{eq:implicitupwind}
			\begin{split}
				\frac{f_{i}^{n+1} - f_{i}^{n}}{\Delta t} + u\frac{f_{i}^{n+1} - f_{i-1}^{n+1}}{\Delta x} = 0
				\\
				f_{i}^{n+1} = f_{i}^{n} - C(f_{i}^{n+1}-f_{i-1}^{n+1})
			\end{split}
		\end{align}
		
		Implicit schemes are in form $AX=B$. \emph{A} is diagonally-dominant matrix of size $N\times N$, where \emph{N} is equal to number of points in discretized domain. \emph{X} is the vector of unknowns at current time step that we are looking for, and \emph{B} is equal to calculated vector from previous time step. When we apply \ref{eq:implicitupwind} to all points in discretized domain, we get lower bidiagonal matrix as in Hoffman and Chiang  \cite{hoffmann_2000}. The general form of \emph{N}-size matrix is shown in \ref{eq:implicitmatrix}.
	
		\begin{equation}
			\label{eq:implicitmatrix}
			\begin{bmatrix}
				1+C    & 0      &        & \cdots &     & 0      \\ 
				-C     & 1+C    &        &        &     &        \\ 
				0      & -C     & \ddots &        &     & \vdots \\ 
				\vdots &        & \ddots &        &     &        \\
					   &        &        & -C     & 1+C & 0      \\ 
				0      & \cdots &        & 0      &  -C & 1+C 
			\end{bmatrix}
			\times 
			\begin{bmatrix}
				f_{1} \\ f_{2} \\ \\ \vdots \\ \\ f_{N}
			\end{bmatrix}^{n+1}
			=
			\begin{bmatrix}
				f_{1} + Cf_{0}^{n+1} \\ f_{2} \\ \\ \vdots \\  \\ f_{N}
			\end{bmatrix}^{n}
		\end{equation}
	
		Nevertheless this is an implicit formula, it can be solved without using complex and time consuming matrix operations, like LU decomposition or Thomas algorithm. Because of the form of this matrix, in considered case, where acceleration $u > 0$, we can use forward substitution to linearise equation to \ref{eq:implicitlinear}.
	
		\begin{equation}
			\label{eq:implicitlinear}
			f_{i}^{n+1}=\frac{f_{i}^{n}+Cf_{i-1}^{n+1}}{1+C},\quad for \quad i=1,2,3,...,N-1
		\end{equation}
	
		
		
		From \ref{fig:implicit-stencil} we can notice, that currently calculated point relays on points, that already have been calculated or obtained from boundary condition. Thus, it is possible to solve that equation, because values $f_{0}^{n+1}$ and $f_{N}^{n+1}$ are given as boundary values.
	
		\begin{figure}[ht]
			\centering
			\begin{tikzpicture}
				\draw[dashed] (-4,1.2) -- (4,1.2);
				\node[black] at (-4.5,0.5) {$n+1$};
				\draw[dashed] (-4,-0.2) -- (4,-0.2);
				\node[black] at (-4.5,-0.9) {$n$};
				\draw[dashed] (-4,-1.6) -- (4,-1.6);
				
				\node[black] at (-1.5,-2) {$i-1$};
				\node[black] at (0,-2) {$i$};
				\node[black] at (+1.5,-2) {$i+1$};
				
				\stencilpt[pattern=north west lines, pattern color=blue]{-1.5,0.5}{i-1}{};
				\stencilpt[green,pattern=crosshatch, pattern color=green]{0,0.5}{i}{};
				\stencilpt[gray,pattern=dots, pattern color=gray]{+1.5,0.5}{i+1}{};
				\stencilpt[gray,pattern=dots, pattern color=gray]{-1.5,-0.9}{j-1}{};
				\stencilpt[pattern=north west lines, pattern color=blue]{0,-0.9}{j}{};
				\stencilpt[gray,pattern=dots, pattern color=gray]{+1.5,-0.9}{j+1}{};
				
				\draw[line width=0.5mm,->,>=stealth] (i-1)--(i);
				\draw[line width=0.5mm,->,>=stealth] (j)--(i);
			\end{tikzpicture}
			\caption{Stencil for forward substitution Implicit Upwind scheme.}
			\label{fig:implicit-stencil}
		\end{figure}
	
		\begin{figure}[ht]
			\centering
			\begin{tikzpicture}
			\draw[dashed] (-4,1.2) -- (4,1.2);
			\node[black] at (-4.5,0.5) {$n+1$};
			\draw[dashed] (-4,-0.2) -- (4,-0.2);
			\node[black] at (-4.5,-0.9) {$n$};
			\draw[dashed] (-4,-1.6) -- (4,-1.6);
			
			\node[black] at (-1.5,-2) {$i-1$};
			\node[black] at (0,-2) {$i$};
			\node[black] at (+1.5,-2) {$i+1$};
			
			\stencilpt[green,pattern=crosshatch, pattern color=green]{-1.5,0.5}{i-1}{};
			\stencilpt[pattern=north west lines, pattern color=blue]{0,0.5}{i}{};
			\stencilpt[gray,pattern=dots, pattern color=gray]{+1.5,0.5}{i+1}{};
			\stencilpt[gray,pattern=dots, pattern color=gray]{-1.5,-0.9}{j-1}{};
			\stencilpt[pattern=north west lines, pattern color=blue]{0,-0.9}{j}{};
			\stencilpt[gray,pattern=dots, pattern color=gray]{+1.5,-0.9}{j+1}{};
			
			\draw[line width=0.5mm,->,>=stealth] (i)--(i-1);
			\draw[line width=0.5mm,->,>=stealth] (j)--(i-1);
			\end{tikzpicture}
			\caption{Stencil for backward substitution Implicit Upwind scheme.}
			\label{fig:implicit-stencil-back}
		\end{figure}
	
		Because of bad results (what will be discussed in \ref{ch:results}) produced by this approach, we also introduce backward substitution for this scheme. Formula \ref{eq:implicitlinearback} has to be used with negative Courant numbers. Stencil for this solution is presented on figure \ref{fig:implicit-stencil-back}.
		
		\begin{equation}
		\label{eq:implicitlinearback}
		f_{i-1}^{n+1}=\frac{(1+C)f_{i}^{n+1}-f_{i}^{n}}{C},\quad for \quad i=N,N-1,N-2,...,2
		\end{equation}
		
		Von Neumann stability analysis for this scheme shows, that it is stable for $C \in (-\infty,-1)\cup(0,\infty)$. Therefore, in case, when $C>0$ this method is unconditionally stable for all combinations of $\Delta t$ and $\Delta x$, but we have to be careful when using negative CFLs.
	
	\newpage
	
	\section{Lax-Wendroff method}
		Lax-Wendroff scheme uses central differences of the second order for the spacial derivative. The formula of this method is presented in \ref{eq:laxwendroff}.
		
		\begin{align}
			\label{eq:laxwendroff}
			\begin{split}
				\frac{f_{i}^{n+1} - f_{i}^{n}}{\Delta t} + \frac{u}{2\Delta x}(f_{i+1}^{n} - f_{i-1}^{n})  - \frac{u^{2}\Delta t}{2\Delta x^{2}}(f_{i+1}^{n} -2f_{i}^{n} + f_{i-1}^{n})= 0
				\\
				f_{i}^{n+1} = f_{i}^{n} - \frac{C}{2}(f_{i+1}^{n}-f_{i-1}^{n}) + \frac{C^{2}}{2}(f_{i+1}^{n} - 2f_{i}^{n} + f_{i-1}^{n})
			\end{split}
		\end{align}
	
		Graphical representation of Lax-Wendroff \ref{fig:laxwendroff-stencil} shows, that this scheme uses three consecutive points from previous time step.
	
		\begin{figure}[ht]
			\centering
			\begin{tikzpicture}
				\draw[dashed] (-4,1.2) -- (4,1.2);
				\node[black] at (-4.5,0.5) {$n+1$};
				\draw[dashed] (-4,-0.2) -- (4,-0.2);
				\node[black] at (-4.5,-0.9) {$n$};
				\draw[dashed] (-4,-1.6) -- (4,-1.6);
				
				\node[black] at (-1.5,-2) {$i-1$};
				\node[black] at (0,-2) {$i$};
				\node[black] at (+1.5,-2) {$i+1$};
				
				\stencilpt[gray,pattern=dots, pattern color=gray]{-1.5,0.5}{i-1}{};
				\stencilpt[green,pattern=crosshatch, pattern color=green]{0,0.5}{i}{};
				\stencilpt[gray,pattern=dots, pattern color=gray]{+1.5,0.5}{i+1}{};
				\stencilpt[pattern=north west lines, pattern color=blue]{-1.5,-0.9}{j-1}{};
				\stencilpt[pattern=north west lines, pattern color=blue]{0,-0.9}{j}{};
				\stencilpt[pattern=north west lines, pattern color=blue]{+1.5,-0.9}{j+1}{};
				\draw[line width=0.5mm,->,>=stealth] (j-1)--(i);
				\draw[line width=0.5mm,->,>=stealth] (j)--(i);
				\draw[line width=0.5mm,->,>=stealth] (j+1)--(i);
			\end{tikzpicture}
			\caption{Stencil for Lax-Wendroff scheme.}
			\label{fig:laxwendroff-stencil}
		\end{figure}

	Lax-Wendroff scheme is second order accurate in time and space $\mathcal{O}(\Delta t^{2}, \Delta x^{2})$. It is stable for $C \le 1$.
	
	\section{Richtmyer method}
		Scheme presented in this section belongs to the family of multi-step methods. It consists of two steps, first step is called \emph{prediction} and the second one is called \emph{correction}. As discussed in Hoffman and Chiang \cite{hoffmann_2000}, first step is calculated using Lax's method \ref{eq:richtmyerprediction}, and then the correction is made with the use of midpoint leapfrog \ref{eq:richtmyercorrection}.\
		
		\begin{equation}\label{eq:richtmyerprediction}
			f_{i}^{n+\frac{1}{2}} = \frac{1}{2}(f_{i+1}^{n} + f_{i-1}^{n}) - \frac{C}{4}(f_{i+1}^{n}-f_{i-1}^{n})
		\end{equation}

		\begin{align}
			\label{eq:richtmyercorrection}
			\begin{split}
				\frac{f_{i}^{n+1} - f_{i}^{n}}{\Delta t} + u\frac{f_{i+1}^{n+\frac{1}{2}} - f_{i-1}^{n+\frac{1}{2}}}{2\Delta x} = 0
				\\
				f_{i}^{n+1} = f_{i}^{n} - \frac{C}{2}\Big(f_{i+1}^{n+\frac{1}{2}} - f_{i-1}^{n+\frac{1}{2}}\Big)
			\end{split}
		\end{align}
		
		Graphical representation of Richtmyer in figure \ref{fig:richtmyer-stencil} shows which point are used for prediction ($1^{\emph{st}}$ step) and correction ($2^{\emph{nd}}$ step). 
		
		\begin{figure}[ht]
			\centering
			\begin{tikzpicture}
				\draw[dashed] (-4,1.2) -- (4,1.2);
				\node[black] at (-4.5,0.5) {$n+1$};
				\draw[dashed] (-4,-0.2) -- (4,-0.2);
				\node[black] at (-4.5,-0.9) {$n+\frac{1}{2}$};
				\draw[dashed] (-4,-1.6) -- (4,-1.6);
				\node[black] at (-4.5,-2.3) {$n$};
				\draw[dashed] (-4,-3) -- (4,-3);
				
				\node[black] at (-3,-3.4) {$i-2$};
				\node[black] at (-1.5,-3.4) {$i-1$};
				\node[black] at (0,-3.4) {$i$};
				\node[black] at (+1.5,-3.4) {$i+1$};
				\node[black] at (3,-3.4) {$i+2$};
				
				\stencilpt[gray,pattern=dots, pattern color=gray]{-3,0.5}{i-2}{};
				\stencilpt[gray,pattern=dots, pattern color=gray]{-1.5,0.5}{i-1}{};
				\stencilpt[green,pattern=crosshatch, pattern color=green]{0,0.5}{i}{};
				\stencilpt[gray,pattern=dots, pattern color=gray]{+1.5,0.5}{i+1}{};
				\stencilpt[gray,pattern=dots, pattern color=gray]{+3,0.5}{i+2}{};
				
				\stencilpt[pattern=north west lines, pattern color=blue]{-1.5,-0.9}{c-1}{};
				\stencilpt[pattern=north west lines, pattern color=blue]{+1.5,-0.9}{c+1}{};
				
				\stencilpt[pattern=north west lines, pattern color=blue]{-3,-2.3}{j-2}{};
				\stencilpt[gray,pattern=dots, pattern color=gray]{-1.5,-2.3}{j-1}{};
				\stencilpt[pattern=north west lines, pattern color=blue]{0,-2.3}{j}{};
				\stencilpt[gray,pattern=dots, pattern color=gray]{+1.5,-2.3}{j+1}{};
				\stencilpt[pattern=north west lines, pattern color=blue]{+3,-2.3}{j+2}{};
				
				\draw[line width=0.5mm,->,>=stealth] (c-1)--(i)
					node [pos=0.4,above,font=\footnotesize] {2};
				\draw[line width=0.5mm,->,>=stealth] (c+1)--(i)
					node [pos=0.4,above,font=\footnotesize] {2};
				\draw[line width=0.5mm,->,>=stealth] (j-2)--(c-1)
					node [pos=0.4,above,font=\footnotesize] {1};
				\draw[line width=0.5mm,->,>=stealth] (j)--(c-1)
					node [pos=0.4,above,font=\footnotesize] {1};
				\draw[line width=0.5mm,->,>=stealth] (j)--(i)
					node [pos=0.5,left,font=\footnotesize] {2};
				\draw[line width=0.5mm,->,>=stealth] (j)--(c+1)
					node [pos=0.4,above,font=\footnotesize] {1};
				\draw[line width=0.5mm,->,>=stealth] (j+2)--(c+1)
					node [pos=0.4,above,font=\footnotesize] {1};
			\end{tikzpicture}
			\caption{Stencil for Richtmyer scheme.}
			\label{fig:richtmyer-stencil}
		\end{figure}
	
	Because of the midpoint leapfrog step used in this scheme, it is second order accurate in time and space $\mathcal{O}(\Delta t^{2}, \Delta x^{2})$. One of the advantages of Richtmyer method is its stability, this scheme stable for $C \le 2$.
	