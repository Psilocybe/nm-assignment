\chapter{Detailed von Neumann stability analysis}
\label{ch:stability}
	The von Neumann stability analysis is very often used when there is a need to determine stability criteria of scheme. It is very easily applied to the solutions of difference equations and gives adequate conditions. The main disadvantage of this method is that it cannot be used for problems with variable coefficients. In our case it is not a problem, because all the coefficients are predetermined and constant.
	
	\section{Implicit upwind scheme}\label{app:implicit}
	To bypass the ambiguity between space index and imaginary unit used in complex numbers we will change notation used to describe value of function in time for specified point. In this appendix, the notation is as follows:
	\begin{equation*}
		f^{n}_{j}:= f(x_{j}, t_{n})
	\end{equation*}
	
	Solving the advection equation \ref{eq:advection} with the first-order backward difference both in time and space in the approximation of PDE, leads to:
	
	\begin{equation}
		\label{app:for:implicitUpwind_first}
		\frac{f_j^{n+1} - f_j^n}{\Delta t} + u\frac{f_j^{n+1} - f_{j-1}^{n+1}}{\Delta x} = \mathcal{O}(\Delta t, \Delta x)
	\end{equation}
	
	Rearranging the terms of \ref{app:for:implicitUpwind_first} gives
	
	\begin{equation}
		\label{app:for:implicitUpwind_solution}
		f_j^n = -Cf_{j-1}^{n+1} + (1+C)f_j^{n+1}
	\end{equation}
	where $C=u\frac{\Delta t}{\Delta x}$ is known as the \emph{CFL} or \emph{Courant} number. To solve the implicit formula \ref{app:for:implicitUpwind_solution}, we have to solve the linear equation system of form $Af^{n+1} = f^n$, where $A$ is a matrix of size $N\times N$, $f^{n+1}$ and $f^{n}$ are vectors of size $N$ ($N$ is equal the grid size).
	
	\begin{equation}
		\begin{bmatrix}
			1+C & \hdots & & 0\\
			-C & 1+C & & \vdots \\ 
			\vdots & \ddots & \ddots \\
			0& \hdots & -C & 1+C \\					
		\end{bmatrix}
		\times
		\begin{bmatrix}
			f_1^{n+1} \\
			f_2^{n+1} \\
			\vdots	\\
			f_N^{n+1}\\
		\end{bmatrix}
		=
		\begin{bmatrix}
			f_1^{n} + C f_0^{n+1}\\
			f_2^{n} \\
			\vdots	\\
			f_N^{n}\\
		\end{bmatrix}
	\end{equation}
	
	Implicit upwind scheme is second-order accurate both in time and space. Because of the form of the $A$ matrix, it is possible to solve the problem using forward or backward substitution, depending on $u$ parameter, to linearise equation \ref{app:for:implicitSubstitution}. The only limitation is that we have to know the boundary conditions for each time step $f_0^{n+1}$ and $f_N^{n+1}$.
	
	\begin{align}
		\label{app:for:implicitSubstitution}
		\begin{split}
			\text{forward:} \quad f_j^{n+1} &= \frac{f_j^n + Cf_{j-1}^{n+1}}{1+C} \quad \text{, for $j = 1,2,3,\ldots,N-1$} \\
			\text{backward:} \quad f_{j-1}^{n+1}&= \frac{(1+C)f_j^{n+1} - f_j^{n+1}}{C} \quad \text{, for $j = N, N-1, \ldots, 2$}
		\end{split}
	\end{align}
	
	To perform von Neumann's stability analysis we have to transform our initial problem to the form as in Collis \cite{collis_2005}:
	\begin{equation}
		\label{app:for:vnkeyImp}
		f_j^{(n)} = \sigma^ne^{ikx_j}
	\end{equation}
		
	Here the term $e^{ikx_j}$ is a complex number, and based on the Euler's formula, it can be rewritten as $cos(kx_j) + isin(kx_j)$. Substituting (\ref{app:for:vnkeyImp}) into (\ref{app:for:implicitUpwind_solution}) results in:
	
	\begin{align}
		\begin{split}
		\sigma ^{n+1}e^{ikx_j} =  -C\sigma^{n+1}e^{ikx_{j-1}} + (1+C)\sigma^{n+1}e^{ikx_j}
		\end{split}
	\end{align}
	Being aware that
	\begin{align}
		\begin{split}
			x_{j-1} &= x_j - \Delta x \\
			x_{j+1} &= x_j + \Delta x
		\end{split}
	\end{align}
	
	\noindent
	and dividing both sides of equation by the term $u_j^{(n)} = \sigma^ne^{ikx_j}$, we obtain
	
	\begin{align}
		\begin{split}
			1 &= -\sigma Ce^{-ik \Delta x} + \sigma(1+C)\\ 
			1 &= \sigma \big(1 + C -Ce^{-ik \Delta x}\big)\\
			1 &= \sigma \big(1 + C(1 -e^{-ik \Delta x})\big)\\	
			1 &= \sigma \big(1 + C\big(1- \cos(k\Delta x) + i\sin(k\Delta x)\big)\big) \\
			1 &= \sigma \big(\underbrace{1 + C\big(1- \cos(k\Delta x)\big)}_{\text{Real part}} + \underbrace{iC\sin(k\Delta x)}_{\text{Imaginary part}}\big) \\	
		\end{split}
	\end{align}
	
	The examined method is stable, when the amplification factor is less or equal to 1. The magnitude of the amplification factor is:
	
	\begin{align}
		\begin{split}
			\label{app:for:sigmaImplicit}
			|\sigma|^2 &= \Bigg|\frac{1}{\Big(1 + C\big(1- \cos(k\Delta x)\big)+ iC\sin(k\Delta x)\Big)}\Bigg|^2 \\	
			|\sigma|^2 &= \frac{1}{\Big(1 + C\big(1- \cos(k\Delta x)\big)\Big)^2+ C^2\sin^2(k\Delta x)} \\
			|\sigma|^2 &= \frac{1}{1 + 2C(1+C)(1-\cos(k\Delta x))}
		\end{split}
	\end{align}
	
	Next we have to check the condition $|\sigma|^2 \leq 1$, our equation is presented in a following way:	
	
	\begin{align}
		2C(1+C)(1-\cos(k\Delta x)) &\geq 0
	\end{align} 
		
	Knowing that $ \forall k \quad -1 \le \cos(k\Delta x) \le 1$, the final result is:
	
	\begin{align}	
		\begin{split}			
			2C(1+C)\geq 0 \implies
			\begin{cases}
				C \geq 0 \\
				C \leq -1
			\end{cases}
		\end{split}
	\end{align}
		
	Von Neumann's stability analysis shows, that Implicit Upwind scheme is conditionally stable with following condition $C \in \left(-\infty, -1\right)\cup \left(0, \infty \right)$.