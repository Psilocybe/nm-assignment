\section{Grid convergence}
	Finite-difference solution is convergent, when the decreasing $\Delta x$ and $\Delta t$ causes approaching of the numerical solution to the analytical of the partial differential equation. To reduce the time degradation of solutions and to	minimize the impact of bad Courant numbers, results are calculated for best CFL and time around 5. We extended our measurements to four ascending grid sizes: 100, 200, 400 and 1000 points. Each grid covers domain from $-50$ to $+50$.
	
\subsection{Explicit Upwind}

\begin{figure}
	\centering
	\begin{tikzpicture}
		\tikzsetnextfilename{grid-explicit-upwind}
		\pgfplotsset{footnotesize,samples=10}
		\begin{groupplot}[%
			group style={columns=2, horizontal sep=50pt},
			width=0.5\textwidth,
			yticklabel style={
				/pgf/number format/fixed,
				/pgf/number format/precision=2
			}]
			\nextgroupplot[title = {$Signum function$},
				legend style={column sep=10pt, legend columns=-1, legend to name=grid-explicit-upwind-grouplegend},
				xmin={7}, xmax={10},
				xlabel={$x$}, ylabel={$f(x)$},
				minor y tick num=1, ymajorgrids=true, xmajorgrids=true,
				grid style=dotted]
				\addcustomplot{charts/grid/sign-100-explicit-upwind.tsv}{black}{1}{Exact}
				\addcustomplot{charts/grid/sign-100-explicit-upwind.tsv}{red}{2}{100 points}
				\addcustomplot{charts/grid/sign-200-explicit-upwind.tsv}{green}{2}{200 points}
				\addcustomplot{charts/grid/sign-400-explicit-upwind.tsv}{blue}{2}{400 points}
			\nextgroupplot[title={$Exponential function$},
				xmin={6}, xmax={12},
				xlabel={$x$}, ylabel={$f(x)$},
				minor y tick num=1, ymajorgrids=true, xmajorgrids=true,
				grid style=dotted]
				\addcustomplotnolegend{charts/grid/exp-100-explicit-upwind.tsv}{black}{1}
				\addcustomplotnolegend{charts/grid/exp-100-explicit-upwind.tsv}{red}{2}
				\addcustomplotnolegend{charts/grid/exp-200-explicit-upwind.tsv}{green}{2}
				\addcustomplotnolegend{charts/grid/exp-400-explicit-upwind.tsv}{blue}{2}
		\end{groupplot}
		\node at ($(group c2r1) + (-4.5cm,-4.0cm)$) {\ref{grid-explicit-upwind-grouplegend}};
	\end{tikzpicture}
	\caption{Explicit upwind scheme results for different grid sizes}
	\label{fig:grid-explicit-upwind}
\end{figure}

\begin{table}
	\caption{Convergence for Explicit Upwind scheme.}
	\label{tab:conv-explicit-upwind}
	\centering
	\begin{tabular}{@{}rrrrrr@{}}
		\toprule
		\multicolumn{6}{c}{Explicit Upwind - Signum} \\
		\midrule
		& \multicolumn{2}{c}{$L^{1}$} & \multicolumn{2}{c}{$L^{2}$} & \multicolumn{1}{c}{$L^{\infty}$}\\
		$\Delta x$ & Error & Rate & Error & Rate & Error \\ [0.5ex]
		\midrule
			\emph{1.01010} & 0.0004812 & -       & 0.0080879 & -       & 0.0080879 \\
			\emph{0.50251} & 0.0002696 & 1.78486 & 0.0052577 & 1.53829 & 0.0052560 \\
			\emph{0.25063} & 0.0001887 & 1.42872 & 0.0034880 & 1.50735 & 0.0034876 \\
			\emph{0.10010} & 0.0003078 & 0.61306 & 0.0026811 & 1.30095 & 0.0026532 \\
		\midrule
		\multicolumn{6}{c}{Explicit Upwind - Exponential} \\
		\midrule
			\emph{1.01010} & 0.0000641 & -       & 0.0036748 & -       & 0.0032056 \\
			\emph{0.50251} & 0.0000389 & 1.64781 & 0.0030156 & 1.21860 & 0.0021496 \\
			\emph{0.25063} & 0.0000188 & 2.06915 & 0.0021317 & 1.41465 & 0.0010946 \\
			\emph{0.10010} & 0.0000262 & 0.71756 & 0.0046845 & 0.45505 & 0.0015263 \\
		\bottomrule
	\end{tabular}
\end{table}

On figure \ref{fig:grid-explicit-upwind} we can observe, that when grid is more dense, the plot of numerical values overlays exact solution more. However, from additional data for grid size equal to 1000 presented in table \ref{tab:conv-explicit-upwind}, we can observe another interesting fact. When domain is divided into too many discretization points, round-off error appears. Basically what it means, is that we no longer increase accuracy of the scheme. Small calculation errors caused by inaccuracy of machine take precedence over truncation errors. Although, for grid sizes 100, 200 and 400 the scheme is convergent, because with the decrease of spatial step, numerical errors tend to zero. For small discretizations, the Explicit Upwind scheme's error decreases almost linearly as it is first-order accurate in space.

\subsection{Implicit Upwind}

\begin{figure}
	\centering
	\begin{tikzpicture}
		\tikzsetnextfilename{grid-implicit-upwind}
		\pgfplotsset{footnotesize,samples=10}
		\begin{groupplot}[%
			group style={columns=2, horizontal sep=50pt},
			width=0.5\textwidth,
			yticklabel style={
				/pgf/number format/fixed,
				/pgf/number format/precision=2
			}]
			\nextgroupplot[title = {$Signum function$},
				legend style={column sep=10pt, legend columns=-1, legend to name=grid-implicit-upwind-grouplegend},
				xmin={-1}, xmax={18},
				xlabel={$x$}, ylabel={$f(x)$},
				minor y tick num=1, ymajorgrids=true, xmajorgrids=true,
				grid style=dotted]
				\addcustomplot{charts/grid/sign-100-implicit-upwind.tsv}{black}{1}{Exact}
				\addcustomplot{charts/grid/sign-100-implicit-upwind.tsv}{red}{2}{100 points}
				\addcustomplot{charts/grid/sign-200-implicit-upwind.tsv}{green}{2}{200 points}
				\addcustomplot{charts/grid/sign-400-implicit-upwind.tsv}{blue}{2}{400 points}
			\nextgroupplot[title={$Exponential function$},
				xmin={-1}, xmax={18},
				xlabel={$x$}, ylabel={$f(x)$},
				minor y tick num=1, ymajorgrids=true, xmajorgrids=true,
				grid style=dotted]
				\addcustomplotnolegend{charts/grid/exp-100-implicit-upwind.tsv}{black}{1}
				\addcustomplotnolegend{charts/grid/exp-100-implicit-upwind.tsv}{red}{2}
				\addcustomplotnolegend{charts/grid/exp-200-implicit-upwind.tsv}{green}{2}
				\addcustomplotnolegend{charts/grid/exp-400-implicit-upwind.tsv}{blue}{2}
				\end{groupplot}
				\node at ($(group c2r1) + (-4.5cm,-4.0cm)$) {\ref{grid-implicit-upwind-grouplegend}};
		\end{tikzpicture}
	\caption{Implicit upwind scheme results for different grid sizes}
	\label{fig:grid-implicit-upwind}
\end{figure}

\begin{table}
	\caption{Convergence for Implicit Upwind scheme.}
	\label{tab:conv-implicit-upwind}
	\centering
	\begin{tabular}{@{}rrrrrr@{}}
		\toprule
		\multicolumn{6}{c}{Implicit Upwind - Signum} \\
		\midrule
		& \multicolumn{2}{c}{$L^{1}$} & \multicolumn{2}{c}{$L^{2}$} & \multicolumn{1}{c}{$L^{\infty}$}\\
		$\Delta x$ & Error & Rate & Error & Rate & Error \\ [0.5ex]
		\midrule
		\emph{1.01010} & 0.0000219 & -       & 0.0092610 & -       & 0.0009261 \\
		\emph{0.50251} & 0.0000118 & 1.85593 & 0.0056870 & 1.62845 & 0.0216856 \\
		\emph{0.25063} & 0.0000056 & 2.10714 & 0.0025770 & 2.20683 & 0.0011770 \\
		\emph{0.10010} & 0.0000110 & 0.50909 & 0.0047772 & 0.53900 & 0.0042207 \\
		\midrule
		\multicolumn{6}{c}{Implicit Upwind - Exponential} \\
		\midrule
		\emph{1.01010} & 0.0000730 & -       & 0.0042050 & -       & 0.0003663 \\
		\emph{0.50251} & 0.0000470 & 1.55319 & 0.0036407 & 1.15500 & 0.0025971 \\
		\emph{0.25063} & 0.0000262 & 1.79389 & 0.0017101 & 2.12894 & 0.0000364 \\
		\emph{0.10010} & 0.0000744 & 0.35215 & 0.0132765 & 0.12881 & 0.0043104 \\
		\bottomrule
	\end{tabular}
\end{table}

Obtained results are comparable to previously described Explicit Upwind scheme. Increase in accuracy is almost linear. From figure \ref{fig:grid-implicit-upwind} we can see, that for smaller grid sizes this scheme is convergent (look at table \ref{tab:conv-implicit-upwind} $L^{1}$ column). For very high grid size, similar behaviour occurs - many small round-off errors are accumulating and harming the results. From the results we can observe, that the gain is greater for wave based on exponential function.

\subsection{Lax-Wendroff}

\begin{figure}
	\centering
	\begin{tikzpicture}
		\tikzsetnextfilename{grid-lax-wendroff}
		\pgfplotsset{footnotesize,samples=10}
		\begin{groupplot}[%
			group style={columns=2, horizontal sep=50pt},
			width=0.5\textwidth,
			yticklabel style={
				/pgf/number format/fixed,
				/pgf/number format/precision=2
			}]
			\nextgroupplot[title = {$Signum function$},
				legend style={column sep=10pt, legend columns=-1, legend to name=grid-lax-wendroff-grouplegend},
				xmin={8}, xmax={10},
				xlabel={$x$}, ylabel={$f(x)$},
				minor y tick num=1, ymajorgrids=true, xmajorgrids=true,
				grid style=dotted]
				\addcustomplot{charts/grid/sign-100-lax-wendroff.tsv}{black}{1}{Exact}
				\addcustomplot{charts/grid/sign-100-lax-wendroff.tsv}{red}{2}{100 points}
				\addcustomplot{charts/grid/sign-200-lax-wendroff.tsv}{green}{2}{200 points}
				\addcustomplot{charts/grid/sign-400-lax-wendroff.tsv}{blue}{2}{400 points}
			\nextgroupplot[title={$Exponential function$},
				xmin={5}, xmax={12},
				xlabel={$x$}, ylabel={$f(x)$},
				minor y tick num=1, ymajorgrids=true, xmajorgrids=true,
				grid style=dotted]
				\addcustomplotnolegend{charts/grid/exp-100-lax-wendroff.tsv}{black}{1}
				\addcustomplotnolegend{charts/grid/exp-100-lax-wendroff.tsv}{red}{2}
				\addcustomplotnolegend{charts/grid/exp-200-lax-wendroff.tsv}{green}{2}
				\addcustomplotnolegend{charts/grid/exp-400-lax-wendroff.tsv}{blue}{2}
			\end{groupplot}
		\node at ($(group c2r1) + (-4.5cm,-4.0cm)$) {\ref{grid-lax-wendroff-grouplegend}};
		\end{tikzpicture}
	\caption{Lax-Wendroff method results for different grid sizes}
	\label{fig:grid-lax-wendroff}
\end{figure}

\begin{table}
	\caption{Convergence for Lax-Wendroff method.}
	\label{tab:conv-lax-wendroff}
	\centering
	\begin{tabular}{@{}rrrrrr@{}}
		\toprule
		\multicolumn{6}{c}{Lax-Wendroff - Signum} \\
		\midrule
		& \multicolumn{2}{c}{$L^{1}$} & \multicolumn{2}{c}{$L^{2}$} & \multicolumn{1}{c}{$L^{\infty}$}\\
		$\Delta x$ & Error & Rate & Error & Rate & Error  \\ [0.5ex]
		\midrule
		\emph{1.01010} & 0.0000016 & -       & 0.0127332 & -       & 0.0121063 \\
		\emph{0.50251} & 0.0000009 & 1.77778 & 0.0282271 & 0.45110 & 0.0269140 \\
		\emph{0.25063} & 0.0000004 & 2.25000 & 0.0541267 & 0.52150 & 0.0518505 \\
		\emph{0.10010} & 0.0000004 & 1.00000 & 0.3729260 & 0.14514 & 0.3701940 \\
		\midrule
		\multicolumn{6}{c}{Lax-Wendroff - Exponential} \\
		\midrule
		\emph{1.01010} & 0.0000700 & -       & 0.0036046 & -       & 0.0028344 \\
		\emph{0.50251} & 0.0000264 & 2.65152 & 0.0020756 & 1.73665 & 0.0012680 \\
		\emph{0.25063} & 0.0000069 & 3.82609 & 0.0007805 & 2.65932 & 0.0003476 \\
		\emph{0.10010} & 0.0000039 & 1.76923 & 0.0006979 & 1.11836 & 0.0001984 \\
		\bottomrule
	\end{tabular}
\end{table}

This second-order accurate scheme shows more gain in accuracy with increase of number of points, when compared to Upwind methods. In table \ref{tab:conv-lax-wendroff} we can observe, that norms are much smaller, and the dispersion is almost not visible when we take a look at figure \ref{fig:grid-lax-wendroff}.

\subsection{Richtmyer}

\begin{figure}
	\centering
	\begin{tikzpicture}
		\tikzsetnextfilename{grid-richtmyer}
		\pgfplotsset{footnotesize,samples=10}
		\begin{groupplot}[%
			group style={columns=2, horizontal sep=50pt},
			width=0.5\textwidth,
			yticklabel style={
				/pgf/number format/fixed,
				/pgf/number format/precision=2
			}]
			\nextgroupplot[title = {$Signum function$},
				legend style={column sep=10pt, legend columns=-1, legend to name=grid-richtmyer-grouplegend},
				xmin={6}, xmax={12},
				xlabel={$x$}, ylabel={$f(x)$},
				minor y tick num=1, ymajorgrids=true, xmajorgrids=true,
				grid style=dotted]
				\addcustomplot{charts/grid/sign-100-richtmyer.tsv}{black}{1}{Exact}
				\addcustomplot{charts/grid/sign-100-richtmyer.tsv}{red}{2}{100 points}
				\addcustomplot{charts/grid/sign-200-richtmyer.tsv}{green}{2}{200 points}
				\addcustomplot{charts/grid/sign-400-richtmyer.tsv}{blue}{2}{400 points}
			\nextgroupplot[title={$Exponential function$},
				xmin={6}, xmax={14},
				xlabel={$x$}, ylabel={$f(x)$},
				minor y tick num=1, ymajorgrids=true, xmajorgrids=true,
				grid style=dotted]
				\addcustomplotnolegend{charts/grid/exp-100-richtmyer.tsv}{black}{1}
				\addcustomplotnolegend{charts/grid/exp-100-richtmyer.tsv}{red}{2}
				\addcustomplotnolegend{charts/grid/exp-200-richtmyer.tsv}{green}{2}
				\addcustomplotnolegend{charts/grid/exp-400-richtmyer.tsv}{blue}{2}
			\end{groupplot}
			\node at ($(group c2r1) + (-4.5cm,-4.0cm)$) {\ref{grid-richtmyer-grouplegend}};
		\end{tikzpicture}
	\caption{Richtmyer method results for different grid sizes}
	\label{fig:grid-richtmyer}
\end{figure}

\begin{table}
	\caption{Convergence for Richtmyer method.}
	\label{tab:conv-richtmyer}
	\centering
	\begin{tabular}{@{}rrrrrr@{}}
		\toprule
		\multicolumn{6}{c}{Richtmyer - Signum} \\
		\midrule
		& \multicolumn{2}{c}{$L^{1}$} & \multicolumn{2}{c}{$L^{2}$} & \multicolumn{1}{c}{$L^{\infty}$}\\
		$\Delta x$ & Error & Rate & Error & Rate & Error \\ [0.5ex]
		\midrule
		\emph{1.01010} & 0.0000798 & -       & 0.0044581 & -       & 0.0029921 \\
		\emph{0.50251} & 0.0000905 & 0.88177 & 0.0101197 & 0.44054 & 0.0067969 \\
		\emph{0.25063} & 0.0000874 & 1.03547 & 0.0195489 & 0.51766 & 0.0131465 \\
		\emph{0.10010} & 0.0002446 & 0.35732 & 0.1370920 & 0.14260 & 0.0935358 \\
		\midrule
		\multicolumn{6}{c}{Richtmyer - Exponential} \\
		\midrule
		\emph{1.01010} & 0.0000630 & -       & 0.0031258 & -       & 0.0025346 \\
		\emph{0.50251} & 0.0000384 & 1.64063 & 0.0030094 & 1.03868 & 0.0017508 \\
		\emph{0.25063} & 0.0000123 & 3.12195 & 0.0014207 & 2.11825 & 0.0006271 \\
		\emph{0.10010} & 0.0000065 & 1.89231 & 0.0011729 & 1.21127 & 0.0003327 \\
		\bottomrule
	\end{tabular}
\end{table}

Refining grid for Richtmyer method also gives better results, especially for exponential initialization function. From data in table \ref{tab:conv-richtmyer} Decreasing spatial step for signum function yelled similar results for all examined sizes. Compared to Lax-Wendroff, this method acquitted itself not so well as expected. In figure \ref{fig:grid-richtmyer} we can also observe a small shift in phase for each grid size. Due to different step in space scheme cannot always hit the same points in domain, therefore the shape of plot can vary.
