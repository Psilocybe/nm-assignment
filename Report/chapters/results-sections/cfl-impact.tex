\section{Impact of CFL number}
	The \emph{Courant/CFL} number depends on velocity, size of the mesh and time step. In examined case, when velocity and grid size are constant values, the only way of controlling the Courant number is to change the time step. In this section we will discuss the quality change of results depending on various CFL numbers and explain the matter of numerical viscosity and dispersion. All shown results are calculated for grid size equal to 100 and a time level close to 10.

\subsection{Explicit Upwind}
	To describe the impact of time step size, four different CFL values are used. We will investigate the behaviour of scheme for:
	\begin{enumerate}
		\setlength{\itemsep}{1pt}
		
		\item $\Delta t = 0.57668 \quad  \longrightarrow \quad CFL = 0.999$
		\item $\Delta t = 0.51953 \quad  \longrightarrow \quad CFL = 0.900$
		\item $\Delta t = 0.40408 \quad  \longrightarrow \quad CFL = 0.700$
		\item $\Delta t = 0.11545 \quad  \longrightarrow \quad CFL = 0.200$
	\end{enumerate}
	
	After examining the formula \ref{eq:explicitupwind}, we can observe that for $C=1$ the scheme produces exact solution: 
	
	\begin{equation*}
		f_{i}^{n+1} = f_{i-1}^{n}
	\end{equation*}
	
	So we can assume, that moving away from the highest stable CFL number, this scheme will produce much worse solutions. As we can see on figure \ref{fig:cfl-explicit-upwind}, the error increases with the decreasing value of Courant number. It is more evident for \emph{exponential} case, that the amplitude of wave is decreasing and it's more dissipated to nearby points. It is interesting, that for each CFL the phase of the wave is preserved. The transition point in \emph{signum} or the peak point in \emph{exponential} are in the same place for numerical and analytical solutions.
	
\begin{figure}[ht]
	\centering
	\begin{tikzpicture}
		\tikzsetnextfilename{cfl-explicit-upwind}
		\pgfplotsset{footnotesize,samples=10}
		\begin{groupplot}[%
			group style={columns=2, horizontal sep=50pt},
			width=0.5\textwidth,
			yticklabel style={
				/pgf/number format/fixed,
				/pgf/number format/precision=2
			}]
			 \nextgroupplot[title = {$Signum function$},
				legend style={column sep=10pt, legend columns=-1, legend to name=cfl-explicit-upwind-grouplegend},
				xmin={5}, xmax={30},
				xlabel={$x$}, ylabel={$f(x)$},
				minor y tick num=1, ymajorgrids=true, xmajorgrids=true,
				grid style=dotted]
				\addcustomplot{charts/cfl/sign-100-explicit-upwind-0.999.tsv}{black}{1}{Exact}
				\addcustomplot{charts/cfl/sign-100-explicit-upwind-0.200.tsv}{red}{2}{CFL=0.2}
				\addcustomplot{charts/cfl/sign-100-explicit-upwind-0.700.tsv}{orange}{2}{CFL=0.7}
				\addcustomplot{charts/cfl/sign-100-explicit-upwind-0.900.tsv}{blue}{2}{CFL=0.9}
				\addcustomplot{charts/cfl/sign-100-explicit-upwind-0.999.tsv}{green}{2}{CFL=0.999}
			\nextgroupplot[title={$Exponential function$},
				xmin={5}, xmax={30},
				xlabel={$x$}, ylabel={$f(x)$},
				minor y tick num=1, ymajorgrids=true, xmajorgrids=true,
				grid style=dotted]
				\addcustomplotnolegend{charts/cfl/exp-100-explicit-upwind-0.999.tsv}{black}{1}
				\addcustomplotnolegend{charts/cfl/exp-100-explicit-upwind-0.200.tsv}{red}{2}
				\addcustomplotnolegend{charts/cfl/exp-100-explicit-upwind-0.700.tsv}{orange}{2}
				\addcustomplotnolegend{charts/cfl/exp-100-explicit-upwind-0.900.tsv}{blue}{2}
				\addcustomplotnolegend{charts/cfl/exp-100-explicit-upwind-0.999.tsv}{green}{2}
		\end{groupplot}
		\node at ($(group c2r1) + (-4.5cm,-4.0cm)$) {\ref{cfl-explicit-upwind-grouplegend}};
	\end{tikzpicture}
	\caption{Explicit upwind scheme results for different CFL values}
	\label{fig:cfl-explicit-upwind}
\end{figure}

	First order schemes suffer from dissipation that is introduced by increasing artificial numerical viscosity. It can be calculated by applying Taylor series expansion on \ref{eq:explicitupwind}, leading to \ref{eq:explicit-upwind-viscosity}.
	
\begin{equation}
	\label{eq:explicit-upwind-viscosity}
	\frac{\partial f}{\partial t} + u\frac{\partial f}{\partial x} = \underbrace{(1-C)\frac{u\Delta x}{2}\frac{\partial^{2} f}{\partial x^{2}}}_{\text{Numerical viscosity}}
\end{equation}

	Analysing the formula \ref{eq:explicit-upwind-viscosity} we come to the same conclusions, that when $C \rightarrow 0$, thus the numerical viscosity increases, the solution becomes more and more affected by dissipation. As shown on figure \ref{fig:cfl-explicit-upwind}, when CFL goes to zero, produced curves are more smooth and their amplitude decreases.
	
\subsection{Implicit Upwind}
	Stability of the Implicit Upwind scheme is divided into two separate numerical intervals, thus we will consider two cases for different sets of Courant numbers. Taylor series expansion of \ref{eq:implicitupwind} produces \ref{eq:implicit-upwind-viscosity}.
	
	\begin{equation}
		\label{eq:implicit-upwind-viscosity}
		\frac{\partial f}{\partial t} + u\frac{\partial f}{\partial x} = \underbrace{(1+C)\frac{u\Delta x}{2}\frac{\partial^{2} f}{\partial x^{2}}}_{\text{Numerical viscosity}}
	\end{equation}

	For this scheme we choose following configuration:
	\begin{enumerate}
		\setlength{\itemsep}{1pt}
		
		\item $\Delta t = 0.63498 \quad \text{and} \quad u=-1.75 \quad  \longrightarrow \quad CFL = -1.100$
		\item $\Delta t = 0.57726 \quad \text{and} \quad u=-1.75 \quad  \longrightarrow \quad CFL = -1.000$
		\item $\Delta t = 0.28863 \quad \text{and} \quad u=1.75 \quad  \longrightarrow \quad CFL = 0.500$
		\item $\Delta t = 0.63498 \quad \text{and} \quad u=1.75 \quad  \longrightarrow \quad CFL = 1.100$
	\end{enumerate}

	\paragraph{Positive CFL number.}
		Detailed von Neumann stability analysis \ref{ch:stability} of this scheme shows, that it is unconditionally stable for all combinations of $\Delta x$ and $\Delta t$ for $C > 0$. However, the results presented in figure \ref{fig:cfl-implicit-upwind} are proof for high dissipation in this range. As it can be inferred from \ref{eq:implicit-upwind-viscosity}, when $C \leftarrow 0$ the scheme will be less affected by numerical viscosity. Nevertheless this is an implicit scheme, which compared to explicit schemes requires more computer time to solve advection equation, it produces worse results.
	
	\paragraph{Negative CFL number.}
		To decrease impact of numerical viscosity and allow scheme to produce valuable results, it is necessary to trick it. Because step in time and step in space have to be positive values, we have to change the sign of velocity. Basically, what it means, is that the wave will be travelling the other way - what in fact, can be easily corrected at the end of simulation, by shifting values to the right.
		
\begin{figure}[ht]
	\centering
	\begin{tikzpicture}
		\tikzsetnextfilename{cfl-implicit-upwind}
		\pgfplotsset{footnotesize,samples=10}
		\begin{groupplot}[%
			group style={columns=2, horizontal sep=50pt},
			width=0.5\textwidth,
			yticklabel style={
				/pgf/number format/fixed,
				/pgf/number format/precision=2
			}]
			\nextgroupplot[title = {$Signum function$},
				legend style={column sep=10pt, legend columns=-1, legend to name=cfl-implicit-upwind-grouplegend},
				xmin={5}, xmax={30},
				xlabel={$x$}, ylabel={$f(x)$},
				minor y tick num=1, ymajorgrids=true, xmajorgrids=true,
				grid style=dotted]
				\addcustomplot{charts/cfl/sign-100-implicit-upwind-1.100.tsv}{black}{1}{Exact}
				\addcustomplot{charts/cfl/sign-100-implicit-upwind-0.500.tsv}{red}{2}{CFL=0.5}
				\addcustomplot{charts/cfl/sign-100-implicit-upwind-1.100.tsv}{orange}{2}{CFL=1.1}
				\addcustomplot{charts/cfl/sign-100-implicit-upwind-n1.10.tsv}{blue}{2}{CFL=-1.1}
				\addcustomplot{charts/cfl/sign-100-implicit-upwind-n1.00.tsv}{green}{2}{CFL=-1.0}
			\nextgroupplot[title={$Exponential function$},
				xmin={5}, xmax={30},
				xlabel={$x$}, ylabel={$f(x)$},
				minor y tick num=1, ymajorgrids=true, xmajorgrids=true,
				grid style=dotted]
				\addcustomplotnolegend{charts/cfl/exp-100-implicit-upwind-1.100.tsv}{black}{1}
				\addcustomplotnolegend{charts/cfl/exp-100-implicit-upwind-0.500.tsv}{red}{2}
				\addcustomplotnolegend{charts/cfl/exp-100-implicit-upwind-1.100.tsv}{orange}{2}
				\addcustomplotnolegend{charts/cfl/exp-100-implicit-upwind-n1.10.tsv}{blue}{2}
				\addcustomplotnolegend{charts/cfl/exp-100-implicit-upwind-n1.00.tsv}{green}{2}
			\end{groupplot}
			\node at ($(group c2r1) + (-4.5cm,-4.0cm)$) {\ref{cfl-implicit-upwind-grouplegend}};
	\end{tikzpicture}
	\caption{Implicit upwind scheme results for different CFL values}
	\label{fig:cfl-implicit-upwind}
\end{figure}

	The second approach gives us much more accurate solutions, less dissipated and with smaller truncation errors. For positive CFLs the amplitudes of waves are not satisfying, but using negative ones, results in quite accurate solutions. The only problem is that we lose unconditional stability of the scheme.
	
\subsection{Lax-Wendroff}
	This method uses second-order accurate approximations of PDE, so we should expect dispersion errors in our results. Equation \ref{eq:lax-wendroff-dispersion} obtained from \ref{eq:laxwendroff} clearly defines the value of numerical dispersion.
	
\begin{equation}
	\label{eq:lax-wendroff-dispersion}
	\frac{\partial f}{\partial t} + u\frac{\partial f}{\partial x} = \underbrace{(C^{2} - 1)\frac{u\Delta x^{2}}{6}\frac{\partial^{3} f}{\partial x^{3}}}_{\text{Numerical dispersion}}
\end{equation}

This scheme will be examined for following parameters:
\begin{enumerate}
	\setlength{\itemsep}{1pt}
	
	\item $\Delta t = 0.57668 \quad  \longrightarrow \quad CFL = 0.999$
	\item $\Delta t = 0.51953 \quad  \longrightarrow \quad CFL = 0.900$
	\item $\Delta t = 0.40408 \quad  \longrightarrow \quad CFL = 0.700$
	\item $\Delta t = 0.11545 \quad  \longrightarrow \quad CFL = 0.200$
\end{enumerate}

	Figure \ref{fig:cfl-lax-wendroff} presents results for mentioned configuration. In this case, the dissipation of the wave is not present, however for lower values of Courant number other phenomena can be observed. In the points of discontinuities and the curvatures of wave some oscillations appear - in that areas we can see the typical impact of numerical dispersion.

\begin{figure}[ht]
	\centering
	\begin{tikzpicture}
		\tikzsetnextfilename{cfl-lax-wendroff}
		\pgfplotsset{footnotesize,samples=10}
		\begin{groupplot}[%
			group style={columns=2, horizontal sep=50pt},
			width=0.5\textwidth,
			yticklabel style={
				/pgf/number format/fixed,
				/pgf/number format/precision=2
			}]
			\nextgroupplot[title = {$Signum function$},
				legend style={column sep=10pt, legend columns=-1, legend to name=cfl-lax-wendroff-grouplegend},
				xmin={0}, xmax={26},
				xlabel={$x$}, ylabel={$f(x)$},
				minor y tick num=1, ymajorgrids=true, xmajorgrids=true,
				grid style=dotted]
				\addcustomplot{charts/cfl/sign-100-lax-wendroff-0.999.tsv}{black}{1}{Exact}
				\addcustomplot{charts/cfl/sign-100-lax-wendroff-0.200.tsv}{red}{2}{CFL=0.2}
				\addcustomplot{charts/cfl/sign-100-lax-wendroff-0.700.tsv}{orange}{2}{CFL=0.7}
				\addcustomplot{charts/cfl/sign-100-lax-wendroff-0.900.tsv}{blue}{2}{CFL=0.9}
				\addcustomplot{charts/cfl/sign-100-lax-wendroff-0.999.tsv}{green}{2}{CFL=0.999}
			\nextgroupplot[title={$Exponential function$},
				xmin={0}, xmax={26},
				xlabel={$x$}, ylabel={$f(x)$},
				minor y tick num=1, ymajorgrids=true, xmajorgrids=true,
				grid style=dotted]
				\addcustomplotnolegend{charts/cfl/exp-100-lax-wendroff-0.999.tsv}{black}{1}
				\addcustomplotnolegend{charts/cfl/exp-100-lax-wendroff-0.200.tsv}{red}{2}
				\addcustomplotnolegend{charts/cfl/exp-100-lax-wendroff-0.700.tsv}{orange}{2}
				\addcustomplotnolegend{charts/cfl/exp-100-lax-wendroff-0.900.tsv}{blue}{2}
				\addcustomplotnolegend{charts/cfl/exp-100-lax-wendroff-0.999.tsv}{green}{2}
		\end{groupplot}
		\node at ($(group c2r1) + (-4.5cm,-4.0cm)$) {\ref{cfl-lax-wendroff-grouplegend}};
	\end{tikzpicture}
	\caption{Lax-Wendroff method results for different CFL values}
	\label{fig:cfl-lax-wendroff}
\end{figure}

Oscillatory behaviour of scheme can be explained by introduction of third-order derivative, thus an each harmonic wave travels at a different speed. Note, that when the wave has been stabilized, its amplitude is quite close to exact solution. Again, the best results are obtained for CFL numbers close to scheme's stability upper boundary, i.e. close to one.

\subsection{Richtmyer}
	The next described method is likewise second-order accurate, therefore impact of dispersion by an odd-order derivative is also expected. However, as stencil of scheme shows, values at point $f_{i}^{n+1}$ are more bound to their neighbours, so the results should be more accurate. Dispersion of Richtmyer scheme \ref{eq:richtmyerprediction} and \ref{eq:richtmyercorrection} is described as \ref{eq:richtmyer-dispersion}.
	
\begin{equation}
	\label{eq:richtmyer-dispersion}
	\frac{\partial f}{\partial t} + u\frac{\partial f}{\partial x} = \underbrace{(C^{2} - 4)\frac{u\Delta x^{2}}{6}\frac{\partial^{3} f}{\partial x^{3}}}_{\text{Numerical dispersion}}
\end{equation}

Because of better stability conditions, we test this scheme with slightly different CFL numbers:
\begin{enumerate}
	\setlength{\itemsep}{1pt}
	
	\item $\Delta t = 1.15394 \quad  \longrightarrow \quad CFL = 1.999$
	\item $\Delta t = 1.09679 \quad  \longrightarrow \quad CFL = 1.900$
	\item $\Delta t = 0.86589 \quad  \longrightarrow \quad CFL = 1.500$
	\item $\Delta t = 0.28863 \quad  \longrightarrow \quad CFL = 0.500$
\end{enumerate}

This method has similar advantages and drawbacks, when compared to Lax-Wendroff scheme. Despite the fact, that it requires a bit more computation time, as it is dependent on more points to be calculated, the oscillatory behaviour is reduced. The numerical solution reaches desirable amplitude faster and the overall plot is less distorted.

\begin{figure}
	\centering
	\begin{tikzpicture}
		\tikzsetnextfilename{cfl-richtmyer}
		\pgfplotsset{footnotesize,samples=10}
		\begin{groupplot}[%
			group style={columns=2, horizontal sep=50pt},
			width=0.5\textwidth,
			yticklabel style={
				/pgf/number format/fixed,
				/pgf/number format/precision=2
			}]
			\nextgroupplot[title = {$Signum function$},
				legend style={column sep=10pt, legend columns=-1, legend to name=cfl-richtmyer-grouplegend},
				xmin={-16}, xmax={26},
				xlabel={$x$}, ylabel={$f(x)$},
				minor y tick num=1, ymajorgrids=true, xmajorgrids=true,
				grid style=dotted]
				\addcustomplot{charts/cfl/sign-100-richtmyer-1.999.tsv}{black}{1}{Exact}
				\addcustomplot{charts/cfl/sign-100-richtmyer-0.500.tsv}{red}{2}{CFL=0.5}
				\addcustomplot{charts/cfl/sign-100-richtmyer-1.500.tsv}{orange}{2}{CFL=1.5}
				\addcustomplot{charts/cfl/sign-100-richtmyer-1.900.tsv}{blue}{2}{CFL=1.9}
				\addcustomplot{charts/cfl/sign-100-richtmyer-1.999.tsv}{green}{2}{CFL=1.999}
			\nextgroupplot[title={$Exponential function$},
				xmin={-16}, xmax={26},
				xlabel={$x$}, ylabel={$f(x)$},
				minor y tick num=1, ymajorgrids=true, xmajorgrids=true,
				grid style=dotted]
				\addcustomplotnolegend{charts/cfl/exp-100-richtmyer-1.999.tsv}{black}{1}
				\addcustomplotnolegend{charts/cfl/exp-100-richtmyer-0.500.tsv}{red}{2}
				\addcustomplotnolegend{charts/cfl/exp-100-richtmyer-1.500.tsv}{orange}{2}
				\addcustomplotnolegend{charts/cfl/exp-100-richtmyer-1.900.tsv}{blue}{2}
				\addcustomplotnolegend{charts/cfl/exp-100-richtmyer-1.999.tsv}{green}{2}
		\end{groupplot}
		\node at ($(group c2r1) + (-4.5cm,-4.0cm)$) {\ref{cfl-richtmyer-grouplegend}};
	\end{tikzpicture}
	\caption{Richtmyer method results for different CFL values}
	\label{fig:cfl-richtmyer}
\end{figure}

Looking at the figure \ref{fig:cfl-richtmyer}, the same conclusions can be made, that if the CFL number is closer to the stability boundary, the better the results. \\

\begin{table}
	\caption{Error vector norms for sets of Courant numbers.}
	\label{tab:cfl-norm-summary}
	\centering
	\ra{1.3}
	\begin{tabular}{@{}rrrrcrrr@{}}
		\toprule
		\multicolumn{8}{c}{Explicit Upwind} \\
		\midrule
		& \multicolumn{3}{c}{Signum} & \phantom{ph} & \multicolumn{3}{c}{Exponential} \\
		\cmidrule{2-4} \cmidrule{6-8}
		CFL & $L^{1}$ & $L^{2}$ & $L^{\infty}$ & & $L^{1}$ & $L^{2}$ & $L^{\infty}$ \\
		\midrule
		\emph{0.999} & 0.01623 & 0.01611 & 0.01611 & & 0.01281 & 0.00733 & 0.00641 \\
		\emph{0.900} & 1.02637 & 0.54067 & 0.39224 & & 0.68299 & 0.29147 & 0.18442 \\
		\emph{0.700} & 1.83666 & 0.74363 & 0.48785 & & 0.89930 & 0.40180 & 0.35541 \\
		\emph{0.200} & 2.97059 & 0.93381 & 0.47872 & & 1.18098 & 0.46146 & 0.40351 \\
		\midrule
			
		\multicolumn{8}{c}{Implicit Upwind} \\
		\midrule
		\emph{-1.00} & 0.00186 & 0.00185 & 0.00185 & & 0.00146 & 0.00084 & 0.00073 \\
		\emph{-1.10} & 1.10361 & 0.62443 & 0.53368 & & 0.62685 & 0.30370 & 0.25967 \\
		\emph{0.500} & 4.11081 & 1.11234 & 0.52593 & & 1.32124 & 0.49034 & 0.43265 \\
		\emph{1.100} & 4.85328 & 1.20656 & 0.52847 & & 1.37848 & 0.50138 & 0.43771 \\
		\midrule
		
		\multicolumn{8}{c}{Lax-Wendroff} \\ 
		\midrule
		\emph{0.999} & 0.03196 & 0.02525 & 0.02407 & & 0.01390 & 0.00718 & 0.00563 \\
		\emph{0.900} & 0.93026 & 0.48558 & 0.39889 & & 0.63364 & 0.26018 & 0.16103 \\
		\emph{0.700} & 1.72108 & 0.77460 & 0.63020 & & 0.94112 & 0.38423 & 0.29308 \\
		\emph{0.200} & 2.56930 & 0.76231 & 0.47397 & & 1.57575 & 0.49382 & 0.34241 \\
		\midrule
		
		\multicolumn{8}{c}{Richtmyer} \\
		\midrule
		\emph{1.999} & 0.01433 & 0.00801 & 0.00538 & & 0.01132 & 0.00562 & 0.00456 \\
		\emph{1.900} & 1.27089 & 0.71841 & 0.53450 & & 0.81830 & 0.33059 & 0.18320 \\
		\emph{1.500} & 2.09978 & 0.73755 & 0.43030 & & 1.20568 & 0.42074 & 0.24886 \\
		\emph{0.500} & 3.94759 & 1.09285 & 0.59797 & & 1.83700 & 0.52145 & 0.40420 \\
		\bottomrule
	\end{tabular}
\end{table}

Summary table \ref{tab:cfl-norm-summary} shows error norms calculated for each of discussed cases. Norm one, tells us about the error sum across all of the domain. Norm two works almost the same way, but big errors have more impact on it, while small errors are ignored. Uniform norm holds the value of the biggest mistake made by a scheme. What can be seen from it, is that if the value of CFL is well selected it can hide the disadvantages as dispersion and dissipation.