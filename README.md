# README #

Assignment for MSc in CSTE Computational Methods & C++ @ Cranfield University.

## What is this repository for? ##

- Introduction
- Computational Methods Task
- C++ Task
- Contact 

## Introduction ##

In this assignment you are asked to examine the application of numerical schemes discussed in the Computational Methods lectures to a linear advection equation, using C++ Object Oriented and other programming practices discussed in the C++ lectures. In order to do this, we will consider the following problem:

_df/dt \+ u \* df/dx = 0_

in a domain

_x in \[\-50; 50\]_

with

_u = 1\.75_

and two sets of initial/boundary conditions.

First set:

_f (x, 0) = 1/2 \* (sign (x) \+ 1.0)_

_f (\-50; t) = 0_

_f (50; t) = 1_

and second set:

_f (x, 0) = 1/2 \* exp(\-x^2)_

_f (\-50; t) = 0_

_f (50; t) = 0_

The analytical solutions for these initial conditions are given respectively.

First set:

_f (x, t) = 1/2 \* (sign (x \- 1\.75t) \+ 1.0)_-

and second set:

_f (x, t) = 1/2 \* exp (\-(x \- 1\.75t)^2)_

## Computational Methods Task ##

- Write a C++ program which solves the above problem with the prescribed initial and boundary conditions on a uniform grid containing 100 points in x, using the following schemes:
    + Explicit Upwind
    + Implicit Upwind
    + Lax-Wendro
    + Richtmyer multi\-step

- Solve the problem and output the numerical and exact solutions at time levels:
    + t = 5
    + t = 10.
- Compare the analytical and numerical solutions. How do the numerical solutions vary with the Courant number? How does the numerical solution change in time. Comparisons should be both qualitative and quantitative.
- Increase the grid size to 200 points and then 400 points in x. Examine the grid convergence of numerical solutions obtained at t = 5.
- Explain the behaviour of the numerical solutions in terms of the expected properties of the schemes involved. In particular for the implicit upwind scheme, study the accuracy and stability properties in depth and include your detailed mathematical calculations in the Appendix of your report.

## C++ Task ##

Think about the design of your solution before jumping into coding.

- What classes (abstractions) will you use?
- What will be the separation of responsibilities into the chosen classes
  i.e. what is each class going to be responsible for doing?
- What will be the data/methods encapsulation for each class?
- What are the relationships between the chosen classes:
    + aggregation ('has a' or 'contains'),
    + inheritance ('is a kind of'),
    + association ('uses' - one class may use objects of another class in the implementation of its methods)?

Some desirables:

- Try to adhere to the SOLID principles for effective design (see BB pages for C++).
- Try to use the Standard Library components where appropriate (containers, iterators, algorithms, numerics). This is well
  designed, thoroughly tested, robust and efficient code
- Try to use exceptions to deal with exceptional conditions and catch them at an appropriate higher level.
- Try not to overcomplicate things. Keep things simple but effective.

Documentation is required (see Vector/Matrix classes used in the module for examples of this). 
Doxygen documentation for the classes and concise in line comments in the functions/methods to explain what chunks of code are doing.
Finally, remember there is no right or wrong answer when it comes to the design but there are better and worse designs. In terms of software, you are aiming for a clean and effective solution to the problem.

## Who do I talk to? ##

* Bartłomiej Szostek (B.Szostek@cranfield.ac.uk)